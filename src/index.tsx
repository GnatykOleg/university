import './styles/index.scss';

import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';

import App from './app/app';
import store from './redux/store';

const rootContainer = document.getElementById('root') as HTMLElement;

const root = ReactDOM.createRoot(rootContainer);

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
);
