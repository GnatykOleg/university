import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';
import { IFindAllEntitiesResponse } from 'types/api/common/find-all-entities.response.interface';
import { ILector } from 'types/api/lectors/lector.interface';
import { IBaseReduxInitialState } from 'types/redux/base-redux-inital-state.interface';
import { PAGINATION, REDUX_SLICE_NAMES } from 'utils';

import * as lectorsOperations from './lectors.operations';

export interface ILectorsInitialState extends IBaseReduxInitialState {
  totalPages: number;
  lectors: ILector[] | [];
}

const initialState: ILectorsInitialState = {
  totalPages: 0,
  lectors: [],
  isLoading: false,
  isError: null,
};

export const lectorsSlice = createSlice({
  name: REDUX_SLICE_NAMES.LECTORS,

  initialState,
  reducers: {},

  extraReducers: (builder) => {
    builder
      // Find all lectors
      .addCase(lectorsOperations.findAllLectors.pending, (state) => {
        state.isLoading = true;
        state.isError = null;
      })
      .addCase(
        lectorsOperations.findAllLectors.fulfilled,
        (
          state,
          { payload }: PayloadAction<IFindAllEntitiesResponse<ILector>>,
        ) => {
          state.isLoading = false;
          state.totalPages = Math.ceil(payload.total / PAGINATION.PER_PAGE);
          state.lectors = payload.data;
        },
      )
      .addCase(
        lectorsOperations.findAllLectors.rejected,
        (state, { payload }: PayloadAction<string | undefined>) => {
          state.isLoading = false;
          state.isError = payload;
        },
      )

      // Create lector
      .addCase(lectorsOperations.createLector.pending, (state) => {
        state.isLoading = true;
        state.isError = null;
      })
      .addCase(
        lectorsOperations.createLector.fulfilled,
        (state, { payload }: PayloadAction<ILector>) => {
          state.isLoading = false;
          state.lectors = [...state.lectors, payload];
        },
      )
      .addCase(
        lectorsOperations.createLector.rejected,
        (state, { payload }: PayloadAction<string | undefined>) => {
          state.isLoading = false;
          state.isError = payload;
        },
      )

      // Update lector
      .addCase(lectorsOperations.updateLector.pending, (state) => {
        state.isLoading = true;
        state.isError = null;
      })
      .addCase(
        lectorsOperations.updateLector.fulfilled,
        (state, { payload }: PayloadAction<ILector>) => {
          state.isLoading = false;

          const lectorIndex = state.lectors.findIndex(
            (lector) => lector.id === payload.id,
          );

          if (lectorIndex !== -1) state.lectors[lectorIndex] = payload;
        },
      )
      .addCase(
        lectorsOperations.updateLector.rejected,
        (state, { payload }: PayloadAction<string | undefined>) => {
          state.isLoading = false;
          state.isError = payload;
        },
      );
  },
});

export default lectorsSlice.reducer;
