import { createAsyncThunk } from '@reduxjs/toolkit';
import * as lectorsApi from 'api/lectors.api';
import { AxiosResponse } from 'axios';
import { IFindAllEntitiesResponse } from 'types/api/common/find-all-entities.response.interface';
import { IUpdateEntityRequest } from 'types/api/common/update-entity.request.interface';
import { ICreateLectorRequest } from 'types/api/lectors/create-lector.request.interface';
import { ILector } from 'types/api/lectors/lector.interface';
import { IFindAllQueries } from 'types/api/queries/find-all-queries.type';
import { generateAxiosErrorMessage } from 'utils';

export const findAllLectors = createAsyncThunk<
  IFindAllEntitiesResponse<ILector>,
  IFindAllQueries,
  { rejectValue: string }
>(
  'lectors/find-all',
  async (
    { sortByField, sortByOrder, page, findByInputValue, findBySelectValue },
    { rejectWithValue },
  ) => {
    try {
      const { data }: AxiosResponse<IFindAllEntitiesResponse<ILector>> =
        await lectorsApi.findAllLectors({
          sortByField,
          sortByOrder,
          page,
          findByInputValue,
          findBySelectValue,
        });

      return data;
    } catch (error) {
      const errorMessage = generateAxiosErrorMessage(error);

      return rejectWithValue(errorMessage);
    }
  },
);

export const createLector = createAsyncThunk<
  ILector,
  ICreateLectorRequest,
  { rejectValue: string }
>('lectors/create', async (lectorFormData, { rejectWithValue, getState }) => {
  try {
    const { data }: AxiosResponse<ILector> =
      await lectorsApi.createLector(lectorFormData);

    return data;
  } catch (error) {
    const errorMessage = generateAxiosErrorMessage(error);

    return rejectWithValue(errorMessage);
  }
});

export const updateLector = createAsyncThunk<
  ILector,
  IUpdateEntityRequest<ICreateLectorRequest>,
  { rejectValue: string }
>('lectors/update', async ({ payload, id }, { rejectWithValue }) => {
  try {
    const { data }: AxiosResponse<ILector> = await lectorsApi.updateLector({
      payload,
      id,
    });

    return data;
  } catch (error) {
    const errorMessage = generateAxiosErrorMessage(error);

    return rejectWithValue(errorMessage);
  }
});
