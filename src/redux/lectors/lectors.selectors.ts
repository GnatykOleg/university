import { ILector } from 'types/api/lectors/lector.interface';
import { IStore } from 'types/redux/redux-store.interface';

export const getIsLectorsError = (store: IStore): any => store.lectors.isError;

export const getIsLectorsLoading = (store: IStore): boolean =>
  store.lectors.isLoading;

export const getAllLectors = (store: IStore): ILector[] | [] =>
  store.lectors.lectors;

export const getLectorsTotalPages = (store: IStore): number =>
  store.lectors.totalPages;
