import { createAsyncThunk } from '@reduxjs/toolkit';
import * as groupsApi from 'api/groups.api';
import { AxiosResponse } from 'axios';
import { IFindAllEntitiesResponse } from 'types/api/common/find-all-entities.response.interface';
import { IUpdateEntityRequest } from 'types/api/common/update-entity.request.interface';
import { ICreateGroupRequest } from 'types/api/groups/create-group.request.interface';
import { IGroup } from 'types/api/groups/group.interface';
import { IFindAllQueries } from 'types/api/queries/find-all-queries.type';
import { generateAxiosErrorMessage } from 'utils';

export const findAllGroups = createAsyncThunk<
  IFindAllEntitiesResponse<IGroup>,
  IFindAllQueries,
  { rejectValue: string }
>(
  'groups/find-all',
  async (
    { sortByField, sortByOrder, page, findByInputValue, findBySelectValue },
    { rejectWithValue },
  ) => {
    try {
      const { data }: AxiosResponse<IFindAllEntitiesResponse<IGroup>> =
        await groupsApi.findAllGroups({
          sortByField,
          sortByOrder,
          page,
          findByInputValue,
          findBySelectValue,
        });

      return data;
    } catch (error) {
      const errorMessage = generateAxiosErrorMessage(error);

      return rejectWithValue(errorMessage);
    }
  },
);

export const createGroup = createAsyncThunk<
  IGroup,
  ICreateGroupRequest,
  { rejectValue: string }
>('groups/create', async (groupFormData, { rejectWithValue }) => {
  try {
    const { data }: AxiosResponse<IGroup> =
      await groupsApi.createGroup(groupFormData);

    return data;
  } catch (error) {
    const errorMessage = generateAxiosErrorMessage(error);

    return rejectWithValue(errorMessage);
  }
});

export const updateGroup = createAsyncThunk<
  IGroup,
  IUpdateEntityRequest<ICreateGroupRequest>,
  { rejectValue: string }
>('groups/update', async ({ payload, id }, { rejectWithValue }) => {
  try {
    const { data }: AxiosResponse<IGroup> = await groupsApi.updateGroup({
      payload,
      id,
    });

    return data;
  } catch (error) {
    const errorMessage = generateAxiosErrorMessage(error);

    return rejectWithValue(errorMessage);
  }
});
