import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IFindAllEntitiesResponse } from 'types/api/common/find-all-entities.response.interface';
import { IGroup } from 'types/api/groups/group.interface';
import { IBaseReduxInitialState } from 'types/redux/base-redux-inital-state.interface';
import { PAGINATION, REDUX_SLICE_NAMES } from 'utils';

import * as groupsOperations from './groups.operations';

export interface IGroupsInitialState extends IBaseReduxInitialState {
  totalPages: number;
  groups: IGroup[] | [];
}

const initialState: IGroupsInitialState = {
  totalPages: 0,
  groups: [],
  isLoading: false,
  isError: null,
};

export const groupsSlice = createSlice({
  name: REDUX_SLICE_NAMES.GROUPS,

  initialState,
  reducers: {},

  extraReducers: (builder) => {
    builder
      // Find all groups
      .addCase(groupsOperations.findAllGroups.pending, (state) => {
        state.isLoading = true;
        state.isError = null;
      })
      .addCase(
        groupsOperations.findAllGroups.fulfilled,
        (
          state,
          { payload }: PayloadAction<IFindAllEntitiesResponse<IGroup>>,
        ) => {
          state.isLoading = false;
          state.totalPages = Math.ceil(payload.total / PAGINATION.PER_PAGE);
          state.groups = payload.data;
        },
      )
      .addCase(
        groupsOperations.findAllGroups.rejected,
        (state, { payload }: PayloadAction<string | undefined>) => {
          state.isLoading = false;
          state.isError = payload;
        },
      )

      // Create group
      .addCase(groupsOperations.createGroup.pending, (state) => {
        state.isLoading = true;
        state.isError = null;
      })
      .addCase(
        groupsOperations.createGroup.fulfilled,
        (state, { payload }: PayloadAction<IGroup>) => {
          state.isLoading = false;
          state.groups = [...state.groups, payload];
        },
      )
      .addCase(
        groupsOperations.createGroup.rejected,
        (state, { payload }: PayloadAction<string | undefined>) => {
          state.isLoading = false;
          state.isError = payload;
        },
      )

      // Update group
      .addCase(groupsOperations.updateGroup.pending, (state) => {
        state.isLoading = true;
        state.isError = null;
      })
      .addCase(
        groupsOperations.updateGroup.fulfilled,
        (state, { payload }: PayloadAction<IGroup>) => {
          state.isLoading = false;

          const groupIndex = state.groups.findIndex(
            (group) => group.id === payload.id,
          );

          if (groupIndex !== -1) state.groups[groupIndex] = payload;
        },
      )
      .addCase(
        groupsOperations.updateGroup.rejected,
        (state, { payload }: PayloadAction<string | undefined>) => {
          state.isLoading = false;
          state.isError = payload;
        },
      );
  },
});

export default groupsSlice.reducer;
