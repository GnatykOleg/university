import { IGroup } from 'types/api/groups/group.interface';
import { IStore } from 'types/redux/redux-store.interface';

export const getIsGroupsError = (store: IStore): any => store.groups.isError;

export const getIsGroupsLoading = (store: IStore): boolean =>
  store.groups.isLoading;

export const getAllGroups = (store: IStore): IGroup[] | [] =>
  store.groups.groups;

export const getGroupsTotalPages = (store: IStore): number =>
  store.groups.totalPages;
