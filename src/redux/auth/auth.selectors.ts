import { IUser } from 'types/api/auth/user.interface';
import { IStore } from 'types/redux/redux-store.interface';

export const getIsAuthError = (store: IStore): any => store.auth.isError;

export const getIsAuthLoading = (store: IStore): boolean =>
  store.auth.isLoading;

export const getCurrentUser = (store: IStore): IUser | null => store.auth.user;
