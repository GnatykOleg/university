import { createAsyncThunk } from '@reduxjs/toolkit';
import * as authApi from 'api/auth.api';
import { AxiosResponse } from 'axios';
import { IUser } from 'types/api/auth/user.interface';
import { generateAxiosErrorMessage, getAccessToken } from 'utils';

export const getUser = createAsyncThunk<
  IUser,
  undefined,
  { rejectValue: string }
>('auth/get-user', async (_, { rejectWithValue }) => {
  const accessToken = getAccessToken();

  if (!accessToken) return rejectWithValue('User not found');

  try {
    const { data }: AxiosResponse<IUser> = await authApi.getUser();

    return data;
  } catch (error: any) {
    const errorMessage = generateAxiosErrorMessage(error);

    return rejectWithValue(errorMessage);
  }
});
