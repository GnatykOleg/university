import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';
import { IUser } from 'types/api/auth/user.interface';
import { REDUX_SLICE_NAMES, removeAccessToken } from 'utils';

import * as authOperations from './auth.operations';

export interface IAuthInitialState {
  user: null | IUser;
  isLoading: boolean;
  isError: any;
}

const initialState: IAuthInitialState = {
  user: null,
  isLoading: false,
  isError: null,
};

export const authSlice = createSlice({
  name: REDUX_SLICE_NAMES.AUTH,

  initialState,
  reducers: {
    logOut: (state) => {
      state.user = null;
      removeAccessToken();
    },
  },

  extraReducers: (builder) => {
    builder
      .addCase(
        authOperations.getUser.fulfilled,
        (state, { payload }: PayloadAction<IUser>) => {
          state.user = payload;
          state.isLoading = false;
        },
      )
      .addCase(authOperations.getUser.pending, (state) => {
        state.user = null;
        state.isLoading = true;
        state.isError = null;
      })
      .addCase(
        authOperations.getUser.rejected,
        (state, { payload }: PayloadAction<string | undefined>) => {
          state.user = null;
          state.isLoading = false;
          state.isError = payload;
        },
      );
  },
});

export const { logOut } = authSlice.actions;

export default authSlice.reducer;
