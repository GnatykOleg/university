import { createAsyncThunk } from '@reduxjs/toolkit';
import * as studentsApi from 'api/students.api';
import { AxiosResponse } from 'axios';
import { IFindAllEntitiesResponse } from 'types/api/common/find-all-entities.response.interface';
import { IUpdateEntityRequest } from 'types/api/common/update-entity.request.interface';
import { IFindAllQueries } from 'types/api/queries/find-all-queries.type';
import { ICreateStudentRequest } from 'types/api/students/create-student.request.interface';
import { IStudent } from 'types/api/students/student.interface';
import { generateAxiosErrorMessage } from 'utils';

export const findAllStudents = createAsyncThunk<
  IFindAllEntitiesResponse<IStudent>,
  IFindAllQueries,
  { rejectValue: string }
>(
  'students/find-all',
  async (
    { sortByField, sortByOrder, page, findByInputValue, findBySelectValue },
    { rejectWithValue },
  ) => {
    try {
      const { data }: AxiosResponse<IFindAllEntitiesResponse<IStudent>> =
        await studentsApi.findAllStudents({
          sortByField,
          sortByOrder,
          page,
          findByInputValue,
          findBySelectValue,
        });

      return data;
    } catch (error) {
      const errorMessage = generateAxiosErrorMessage(error);

      return rejectWithValue(errorMessage);
    }
  },
);

export const createStudent = createAsyncThunk<
  IStudent,
  ICreateStudentRequest,
  { rejectValue: string }
>('students/create', async (createStudentFormData, { rejectWithValue }) => {
  try {
    const { data }: AxiosResponse<IStudent> = await studentsApi.createStudent(
      createStudentFormData,
    );

    return data;
  } catch (error) {
    const errorMessage = generateAxiosErrorMessage(error);

    return rejectWithValue(errorMessage);
  }
});

export const updateStudent = createAsyncThunk<
  IStudent,
  IUpdateEntityRequest<ICreateStudentRequest>,
  { rejectValue: string }
>('students/update', async ({ payload, id }, { rejectWithValue }) => {
  try {
    const { data }: AxiosResponse<IStudent> = await studentsApi.updateStudent({
      payload,
      id,
    });

    return data;
  } catch (error) {
    const errorMessage = generateAxiosErrorMessage(error);

    return rejectWithValue(errorMessage);
  }
});
