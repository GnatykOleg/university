import { IStudent } from 'types/api/students/student.interface';
import { IStore } from 'types/redux/redux-store.interface';

export const getIsStudentsError = (store: IStore): any =>
  store.students.isError;

export const getIsStudentsLoading = (store: IStore): boolean =>
  store.students.isLoading;

export const getAllStudents = (store: IStore): IStudent[] | [] =>
  store.students.students;

export const getStudentsTotalPages = (store: IStore): number =>
  store.students.totalPages;
