import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IFindAllEntitiesResponse } from 'types/api/common/find-all-entities.response.interface';
import { IStudent } from 'types/api/students/student.interface';
import { IBaseReduxInitialState } from 'types/redux/base-redux-inital-state.interface';
import { PAGINATION, REDUX_SLICE_NAMES } from 'utils';

import * as studentsOperations from './students.operations';

export interface IStudentsInitialState extends IBaseReduxInitialState {
  totalPages: number;
  students: IStudent[] | [];
}

const initialState: IStudentsInitialState = {
  totalPages: 0,
  students: [],
  isLoading: false,
  isError: null,
};

export const studentsSlice = createSlice({
  name: REDUX_SLICE_NAMES.STUDENTS,

  initialState,
  reducers: {},

  extraReducers: (builder) => {
    builder
      // Find all students
      .addCase(studentsOperations.findAllStudents.pending, (state) => {
        state.isLoading = true;
        state.isError = null;
      })
      .addCase(
        studentsOperations.findAllStudents.fulfilled,
        (
          state,
          { payload }: PayloadAction<IFindAllEntitiesResponse<IStudent>>,
        ) => {
          state.isLoading = false;
          state.totalPages = Math.ceil(payload.total / PAGINATION.PER_PAGE);
          state.students = payload.data;
        },
      )
      .addCase(
        studentsOperations.findAllStudents.rejected,
        (state, { payload }: PayloadAction<string | undefined>) => {
          state.isLoading = false;
          state.isError = payload;
        },
      )
      // Create student
      .addCase(studentsOperations.createStudent.pending, (state) => {
        state.isLoading = true;
        state.isError = null;
      })
      .addCase(
        studentsOperations.createStudent.fulfilled,
        (state, { payload }: PayloadAction<IStudent>) => {
          state.isLoading = false;
          state.students = [...state.students, payload];
        },
      )
      .addCase(
        studentsOperations.createStudent.rejected,
        (state, { payload }: PayloadAction<string | undefined>) => {
          state.isLoading = false;
          state.isError = payload;
        },
      )
      // Update Student
      .addCase(studentsOperations.updateStudent.pending, (state) => {
        state.isLoading = true;
        state.isError = null;
      })
      .addCase(
        studentsOperations.updateStudent.fulfilled,
        (state, { payload }: PayloadAction<IStudent>) => {
          state.isLoading = false;
          const studentIndex = state.students.findIndex(
            (student) => student.id === payload.id,
          );
          if (studentIndex !== -1) state.students[studentIndex] = payload;
        },
      )
      .addCase(
        studentsOperations.updateStudent.rejected,
        (state, { payload }: PayloadAction<string | undefined>) => {
          state.isLoading = false;
          state.isError = payload;
        },
      );
  },
});

export default studentsSlice.reducer;
