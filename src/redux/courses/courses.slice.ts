import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IFindAllEntitiesResponse } from 'types/api/common/find-all-entities.response.interface';
import { ICourse } from 'types/api/courses/course.interface';
import { IBaseReduxInitialState } from 'types/redux/base-redux-inital-state.interface';
import { PAGINATION, REDUX_SLICE_NAMES } from 'utils';

import * as coursesOperations from './courses.operations';

export interface ICoursesInitialState extends IBaseReduxInitialState {
  totalPages: number;
  courses: ICourse[] | [];
}

const initialState: ICoursesInitialState = {
  totalPages: 0,
  courses: [],
  isLoading: false,
  isError: null,
};

export const coursesSlice = createSlice({
  name: REDUX_SLICE_NAMES.COURSES,

  initialState,
  reducers: {},

  extraReducers: (builder) => {
    builder
      // Find all courses
      .addCase(coursesOperations.findAllCourses.pending, (state) => {
        state.isLoading = true;
        state.isError = null;
      })
      .addCase(
        coursesOperations.findAllCourses.fulfilled,
        (
          state,
          { payload }: PayloadAction<IFindAllEntitiesResponse<ICourse>>,
        ) => {
          state.isLoading = false;
          state.totalPages = Math.ceil(payload.total / PAGINATION.PER_PAGE);
          state.courses = payload.data;
        },
      )
      .addCase(
        coursesOperations.findAllCourses.rejected,
        (state, { payload }: PayloadAction<string | undefined>) => {
          state.isLoading = false;
          state.isError = payload;
        },
      )

      // Create course
      .addCase(coursesOperations.createCourse.pending, (state) => {
        state.isLoading = true;
        state.isError = null;
      })
      .addCase(
        coursesOperations.createCourse.fulfilled,
        (state, { payload }: PayloadAction<ICourse>) => {
          state.isLoading = false;
          state.courses = [...state.courses, payload];
        },
      )
      .addCase(
        coursesOperations.createCourse.rejected,
        (state, { payload }: PayloadAction<string | undefined>) => {
          state.isLoading = false;
          state.isError = payload;
        },
      )

      // Update course
      .addCase(coursesOperations.updateCourse.pending, (state) => {
        state.isLoading = true;
        state.isError = null;
      })
      .addCase(
        coursesOperations.updateCourse.fulfilled,
        (state, { payload }: PayloadAction<ICourse>) => {
          state.isLoading = false;

          const courseIndex = state.courses.findIndex(
            (course) => course.id === payload.id,
          );

          if (courseIndex !== -1) state.courses[courseIndex] = payload;
        },
      )
      .addCase(
        coursesOperations.updateCourse.rejected,
        (state, { payload }: PayloadAction<string | undefined>) => {
          state.isLoading = false;
          state.isError = payload;
        },
      );
  },
});

export default coursesSlice.reducer;
