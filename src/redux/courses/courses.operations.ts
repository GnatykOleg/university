import { createAsyncThunk } from '@reduxjs/toolkit';
import * as coursesApi from 'api/courses.api';
import { AxiosResponse } from 'axios';
import { IFindAllEntitiesResponse } from 'types/api/common/find-all-entities.response.interface';
import { IUpdateEntityRequest } from 'types/api/common/update-entity.request.interface';
import { ICourse } from 'types/api/courses/course.interface';
import { ICreateCourseRequest } from 'types/api/courses/create-course.request.interface';
import { IFindAllQueries } from 'types/api/queries/find-all-queries.type';
import { generateAxiosErrorMessage } from 'utils';

export const findAllCourses = createAsyncThunk<
  IFindAllEntitiesResponse<ICourse>,
  IFindAllQueries,
  { rejectValue: string }
>(
  'courses/find-all',
  async (
    { sortByField, sortByOrder, page, findByInputValue, findBySelectValue },
    { rejectWithValue },
  ) => {
    try {
      const { data }: AxiosResponse<IFindAllEntitiesResponse<ICourse>> =
        await coursesApi.findAllCourses({
          sortByField,
          sortByOrder,
          page,
          findByInputValue,
          findBySelectValue,
        });

      return data;
    } catch (error) {
      const errorMessage = generateAxiosErrorMessage(error);

      return rejectWithValue(errorMessage);
    }
  },
);

export const createCourse = createAsyncThunk<
  ICourse,
  ICreateCourseRequest,
  { rejectValue: string }
>('courses/create', async (courseFormData, { rejectWithValue }) => {
  try {
    const { data }: AxiosResponse<ICourse> =
      await coursesApi.createCourse(courseFormData);

    return data;
  } catch (error) {
    const errorMessage = generateAxiosErrorMessage(error);

    return rejectWithValue(errorMessage);
  }
});

export const updateCourse = createAsyncThunk<
  ICourse,
  IUpdateEntityRequest<ICreateCourseRequest>,
  { rejectValue: string }
>('courses/update', async ({ payload, id }, { rejectWithValue }) => {
  try {
    const { data }: AxiosResponse<ICourse> = await coursesApi.updateCourse({
      payload,
      id,
    });

    return data;
  } catch (error) {
    const errorMessage = generateAxiosErrorMessage(error);

    return rejectWithValue(errorMessage);
  }
});
