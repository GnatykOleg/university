import { ICourse } from 'types/api/courses/course.interface';
import { IStore } from 'types/redux/redux-store.interface';

export const getIsCoursesError = (store: IStore): any => store.courses.isError;

export const getIsCoursesLoading = (store: IStore): boolean =>
  store.courses.isLoading;

export const getAllCourses = (store: IStore): ICourse[] | [] =>
  store.courses.courses;

export const getCoursesTotalPages = (store: IStore): number =>
  store.courses.totalPages;
