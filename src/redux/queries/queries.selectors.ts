import { FindByInputValueType } from 'types/api/queries/find-by-input-value.type';
import { FindBySelectValueType } from 'types/api/queries/find-by-select-value.type';
import { IOption } from 'types/api/queries/select-option.interface';
import { SortByFieldType } from 'types/api/queries/sort-by-filed.type';
import { SortByOrderType } from 'types/api/queries/sort-by-order.type';
import { IStore } from 'types/redux/redux-store.interface';

export const getPage = (store: IStore): number => store.queries.page;

export const getSortByOrder = (store: IStore): SortByOrderType =>
  store.queries.sortByOrder;

export const getSortByField = (store: IStore): SortByFieldType =>
  store.queries.sortByField;

export const getSortBySelectSelectedOption = (store: IStore): IOption | null =>
  store.queries.sortBySelectSelectedOption;

export const getFindBySelectValue = (store: IStore): FindBySelectValueType =>
  store.queries.findBySelectValue;

export const getFindByInputValue = (store: IStore): FindByInputValueType =>
  store.queries.findByInputValue;

export const getFindBySelectSelectedOption = (store: IStore): IOption | null =>
  store.queries.findBySelectSelectedOption;
