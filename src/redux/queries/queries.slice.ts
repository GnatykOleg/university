import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';
import { FindByInputValueType } from 'types/api/queries/find-by-input-value.type';
import { FindBySelectValueType } from 'types/api/queries/find-by-select-value.type';
import { IOption } from 'types/api/queries/select-option.interface';
import { SortByFieldType } from 'types/api/queries/sort-by-filed.type';
import { SortByOrderType } from 'types/api/queries/sort-by-order.type';
import { REDUX_SLICE_NAMES } from 'utils';

export interface IQueriesInitialState {
  page: number;

  findBySelectValue: FindBySelectValueType;
  findByInputValue: FindByInputValueType;
  findBySelectSelectedOption: IOption | null;

  sortByOrder: SortByOrderType;
  sortByField: SortByFieldType;
  sortBySelectSelectedOption: IOption | null;
}

const initialState: IQueriesInitialState = {
  page: 1,

  findBySelectValue: undefined,
  findByInputValue: undefined,
  findBySelectSelectedOption: null,

  sortByOrder: undefined,
  sortByField: undefined,
  sortBySelectSelectedOption: null,
};

export const sortBySlice = createSlice({
  name: REDUX_SLICE_NAMES.QUERIES,

  initialState,
  reducers: {
    clearQueries: (state) => {
      state.page = 1;

      state.findBySelectValue = undefined;
      state.findByInputValue = undefined;
      state.findBySelectSelectedOption = null;

      state.sortByOrder = undefined;
      state.sortByField = undefined;
      state.sortBySelectSelectedOption = null;
    },

    setPage: (state, { payload }: PayloadAction<number>) => {
      state.page = payload;
    },

    setSortOrder: (state, { payload }: PayloadAction<SortByOrderType>) => {
      state.sortByOrder = payload;
    },
    setSortField: (state, { payload }: PayloadAction<SortByFieldType>) => {
      state.sortByField = payload;
    },
    setSortBySelectSelectedOption: (
      state,
      { payload }: PayloadAction<IOption>,
    ) => {
      state.sortBySelectSelectedOption = payload;
    },

    setFindBySelectValue: (
      state,
      { payload }: PayloadAction<FindBySelectValueType>,
    ) => {
      state.findBySelectValue = payload;
    },

    setFindByInputValue: (
      state,
      { payload }: PayloadAction<FindByInputValueType>,
    ) => {
      state.findByInputValue = payload;
    },

    setFindBySelectSelectedOption: (
      state,
      { payload }: PayloadAction<IOption>,
    ) => {
      state.findBySelectSelectedOption = payload;
    },
  },
});

export const {
  setSortField,
  setSortOrder,
  setSortBySelectSelectedOption,
  setPage,
  setFindBySelectValue,
  setFindByInputValue,
  setFindBySelectSelectedOption,
  clearQueries,
} = sortBySlice.actions;

export default sortBySlice.reducer;
