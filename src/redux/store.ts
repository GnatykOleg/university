import { configureStore } from '@reduxjs/toolkit';
import { REDUX_SLICE_NAMES } from 'utils';

import authSlice from './auth/auth.slice';
import coursesSlice from './courses/courses.slice';
import groupsSlice from './groups/groups.slice';
import lectorsSlice from './lectors/lectors.slice';
import queriesSlice from './queries/queries.slice';
import studentsSlice from './students/students.slice';

const store = configureStore({
  reducer: {
    [REDUX_SLICE_NAMES.AUTH]: authSlice,
    [REDUX_SLICE_NAMES.LECTORS]: lectorsSlice,
    [REDUX_SLICE_NAMES.GROUPS]: groupsSlice,
    [REDUX_SLICE_NAMES.COURSES]: coursesSlice,
    [REDUX_SLICE_NAMES.STUDENTS]: studentsSlice,

    [REDUX_SLICE_NAMES.QUERIES]: queriesSlice,
  },
});

export type RootStateType = ReturnType<typeof store.getState>;

export type AppDispatchType = typeof store.dispatch;

export default store;
