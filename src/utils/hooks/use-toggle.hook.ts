import { useState } from 'react';

const useToggle = (): {
  state: boolean;
  toggle: () => void;
} => {
  const [state, setState] = useState(false);

  const toggle = (): void => setState((state) => !state);

  return { state, toggle };
};

export default useToggle;
