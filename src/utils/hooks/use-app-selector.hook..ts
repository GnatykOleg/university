import { TypedUseSelectorHook, useSelector } from 'react-redux';
import { RootStateType } from 'redux/store';

const useAppSelector: TypedUseSelectorHook<RootStateType> = useSelector;

export default useAppSelector;
