import { useState } from 'react';

const useTogglePasswordVisibility = (): {
  passwordVisibilityToggle: () => void;
  inputType: 'text' | 'password';
} => {
  const [passwordVisibility, setPasswordVisibility] = useState<boolean>(false);

  const passwordVisibilityToggle = (): void =>
    setPasswordVisibility((state) => !state);

  const inputType = passwordVisibility ? 'text' : 'password';

  return { passwordVisibilityToggle, inputType };
};

export default useTogglePasswordVisibility;
