import { useDispatch } from 'react-redux';
import { AppDispatchType } from 'redux/store';

const useAppDispatch: () => AppDispatchType = useDispatch;

export default useAppDispatch;
