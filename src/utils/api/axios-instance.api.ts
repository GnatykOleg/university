import axios, { InternalAxiosRequestConfig } from 'axios';
import { toast } from 'react-toastify';
import {
  generateAxiosErrorMessage,
  getAccessToken,
  removeAccessToken,
} from 'utils';

const axiosConfig = {
  baseURL: 'https://university-api-67ff.onrender.com/api/v1',
};

const authRequestInterceptor = (
  config: InternalAxiosRequestConfig<any>,
): InternalAxiosRequestConfig => {
  const token = getAccessToken();

  if (token) config.headers.Authorization = `Bearer ${token}`;
  else delete config.headers.Authorization;

  return config;
};

const authResponseErrorInterceptor = (error: any): Promise<never> => {
  if (error.response.status === 401) {
    const errorMessage = generateAxiosErrorMessage(error);

    removeAccessToken();

    toast.error(errorMessage);
  }

  return Promise.reject(error);
};

const privateAxiosInstance = axios.create(axiosConfig);

const publicAxiosInstance = axios.create(axiosConfig);

privateAxiosInstance.interceptors.request.use(authRequestInterceptor);

privateAxiosInstance.interceptors.response.use(
  (response) => response,
  authResponseErrorInterceptor,
);

export { privateAxiosInstance, publicAxiosInstance };
