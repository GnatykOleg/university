import { AxiosError, isAxiosError } from 'axios';

interface IAxiosErrorBody {
  error: string;
  message: string | string[];
  statusCode: number;
}

const generateAxiosErrorMessage = (
  error: AxiosError<IAxiosErrorBody> | unknown,
): string => {
  if (!isAxiosError(error)) return 'An unexpected error occurred';

  const errorData = error.response?.data.message;

  const errorMessage: string = Array.isArray(errorData)
    ? errorData.join(', ')
    : errorData;

  return errorMessage;
};

export default generateAxiosErrorMessage;
