export {
  privateAxiosInstance,
  publicAxiosInstance,
} from './api/axios-instance.api';
// Utils => Api
export { default as generateAxiosErrorMessage } from './api/generate-error-message.api';

// Utils => Constants
export { default as FORM_PLACEHOLDERS } from './constants/form-placeholders.constant';
export { default as LOCAL_STORAGE } from './constants/local-storage.constant';
export { default as PAGINATION } from './constants/pagination.constant';
export { default as REDUX_SLICE_NAMES } from './constants/redux-slice-names.constant';
export { default as ROUTES_PATHS } from './constants/routes-paths.constant';

// Utils => Hooks
export { default as useAppDispatch } from './hooks/use-app-dispatch.hook';
export { default as useAppSelector } from './hooks/use-app-selector.hook.';
export { default as useToggle } from './hooks/use-toggle.hook';
export { default as useTogglePasswordVisibility } from './hooks/use-toggle-password-visibility.hook';

// Utils => Local storage
export { default as getAccessToken } from './local-storage/get-access-token.local-storage';
export { default as removeAccessToken } from './local-storage/remove-access-token.local-storage';
export { default as saveAccessToken } from './local-storage/save-access-token.local-storage';

// Utils => Schemas
export { default as generateAgeField } from './schemas/generate-age-field.schema';
export { default as generateConfirmPasswordField } from './schemas/generate-confirm-password-field.schema';
export { default as generateDescriptionField } from './schemas/generate-description-field.schema';
export { default as generateEmailField } from './schemas/generate-email-field.schema';
export { default as generateHoursField } from './schemas/generate-hours-field.schema';
export { default as generateNameField } from './schemas/generate-name-field.schema';
export { default as generatePasswordField } from './schemas/generate-password-field.schema';
export { default as generateSurnameField } from './schemas/generate-surname-field.schema';
