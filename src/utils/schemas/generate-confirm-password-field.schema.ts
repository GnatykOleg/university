import Joi from 'joi';

const generateConfirmPasswordField = ({
  passwordToMatchName,
}: {
  passwordToMatchName: string;
}): Joi.StringSchema<string> =>
  Joi.string()
    .equal(Joi.ref(passwordToMatchName))
    .options({
      messages: {
        'any.only': '"confirmPassword" does not match with password',
      },
    })
    .required();

export default generateConfirmPasswordField;
