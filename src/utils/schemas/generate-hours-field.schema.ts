import Joi from 'joi';

const generateHoursField = ({
  optional,
  min = 100,
  max = 1000,
}: {
  optional: boolean;
  min?: number | undefined;
  max?: number | undefined;
}): Joi.NumberSchema<number> => {
  const base = Joi.number().min(min).max(max);

  return optional ? base.allow('') : base.required();
};

export default generateHoursField;
