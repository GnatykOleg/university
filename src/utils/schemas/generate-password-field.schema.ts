import Joi from 'joi';
import passwordValidator from 'password-validator';

const generatePasswordField = ({
  passwordName,
  min = 5,
  symbols = 1,
  digits = 1,
  optional,
}: {
  passwordName: string;
  min?: number | undefined;
  symbols?: number | undefined;
  digits?: number | undefined;
  optional: boolean;
}): Joi.StringSchema<string> => {
  const message = `The "${passwordName}" should be at least ${min} characters long, contain uppercase letter, ${symbols} symbol, and ${digits} digits`;

  const base = Joi.string()
    .min(min)
    .custom((value, helper) => {
      const validator = new passwordValidator();

      validator
        .is()
        .min(min)
        .has()
        .uppercase(1)
        .has()
        .lowercase(1)
        .has()
        .symbols(symbols)
        .has()
        .digits(digits);

      if (!validator.validate(value))
        return helper.error('password.complexity');

      return value;
    }, 'custom validation')
    .message(message);

  return optional ? base.empty('') : base.required();
};

export default generatePasswordField;
