import Joi from 'joi';

const generateSurnameField = ({
  optional,
  min = 3,
  max = 50,
}: {
  optional: boolean;
  min?: number | undefined;
  max?: number | undefined;
}): Joi.StringSchema<string> => {
  const base = Joi.string().min(min).max(max);

  return optional ? base.empty('') : base.required();
};

export default generateSurnameField;
