import Joi from 'joi';

const generateAgeField = ({
  optional,
  min = 18,
  max = 150,
}: {
  optional: boolean;
  min?: number | undefined;
  max?: number | undefined;
}): Joi.NumberSchema<number> => {
  const base = Joi.number().min(min).max(max);

  return optional ? base.allow('') : base.required();
};

export default generateAgeField;
