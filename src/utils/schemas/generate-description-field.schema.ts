import Joi from 'joi';

const generateDescriptionField = ({
  optional,
  min = 10,
}: {
  optional: boolean;
  min?: number | undefined;
}): Joi.StringSchema<string> => {
  const base = Joi.string().min(min);

  return optional ? base.empty('') : base.required();
};

export default generateDescriptionField;
