import { LOCAL_STORAGE } from 'utils';

const getAccessToken = (): string | null =>
  localStorage.getItem(LOCAL_STORAGE.ACCESS_TOKEN);

export default getAccessToken;
