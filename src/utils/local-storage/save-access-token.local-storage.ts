import { LOCAL_STORAGE } from 'utils';

const saveAccessToken = (accessToken: string): void =>
  localStorage.setItem(LOCAL_STORAGE.ACCESS_TOKEN, accessToken);

export default saveAccessToken;
