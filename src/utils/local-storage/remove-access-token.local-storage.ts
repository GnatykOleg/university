import { LOCAL_STORAGE } from 'utils';

const removeAccessToken = (): void =>
  localStorage.removeItem(LOCAL_STORAGE.ACCESS_TOKEN);

export default removeAccessToken;
