const PAGINATION = Object.freeze({
  PER_PAGE: 10,
});

export default PAGINATION;
