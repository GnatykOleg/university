const ROUTES_PATHS = Object.freeze({
  HOME: '/',

  SIGN_IN: '/sign-in',
  SIGN_UP: '/sign-up',

  FORGOT_PASSWORD: '/forgot-password',
  RESET_PASSWORD: '/reset-password',

  LECTORS: '/lectors',
  COURSES: '/courses',
  GROUPS: '/groups',
  STUDENTS: '/students',
  STUDENTS_DETAIL: '/students/:id',
});

export default ROUTES_PATHS;
