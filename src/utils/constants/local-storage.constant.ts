const LOCAL_STORAGE = Object.freeze({
  ACCESS_TOKEN: 'access-token',
});

export default LOCAL_STORAGE;
