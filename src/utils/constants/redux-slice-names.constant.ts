const REDUX_SLICE_NAMES = Object.freeze({
  AUTH: 'auth',
  LECTORS: 'lectors',
  GROUPS: 'groups',
  COURSES: 'courses',
  STUDENTS: 'students',

  QUERIES: 'queries',
});

export default REDUX_SLICE_NAMES;
