const FORM_PLACEHOLDERS = Object.freeze({
  PASSWORD: '•••••••••••••••',
  EMAIL: 'name@mail.com',
  NAME: 'John',
  SURNAME: 'Smith',
  AGE: '21',

  GROUP_NAME: 'bc-22',
  COURSE_NAME: 'astronomy',
  COURSE_DESCRIPTION: 'Here we teach astronomy',
});

export default FORM_PLACEHOLDERS;
