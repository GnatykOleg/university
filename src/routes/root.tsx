import { createBrowserRouter } from 'react-router-dom';
import { ROUTES_PATHS } from 'utils';

import ForgotPasswordRoute from './auth/forgot-password.route';
import ResetPasswordRoute from './auth/reset-password.route';
import SignInRoute from './auth/sign-in.route';
import SignUpRoute from './auth/sign-up.route';
import ErrorRoute from './common/error.route';
import CoursesRoute from './dashboard/courses.route';
import GroupsRoute from './dashboard/groups.route';
import LectorsRoute from './dashboard/lectors.route';
import StudentDetailRoute from './dashboard/student-detail.route';
import StudentsRoute from './dashboard/students.route';
import PrivateRoute from './protected/private.route';
import PublicRoute from './protected/public.route';

export const router = createBrowserRouter([
  {
    element: <PublicRoute />,
    errorElement: <ErrorRoute />,
    children: [
      {
        path: ROUTES_PATHS.SIGN_IN,
        element: <SignInRoute />,
      },
      {
        path: ROUTES_PATHS.SIGN_UP,
        element: <SignUpRoute />,
      },
      {
        path: ROUTES_PATHS.FORGOT_PASSWORD,
        element: <ForgotPasswordRoute />,
      },
      {
        path: ROUTES_PATHS.RESET_PASSWORD,
        element: <ResetPasswordRoute />,
      },
    ],
  },

  {
    path: ROUTES_PATHS.HOME,
    element: <PrivateRoute />,
    errorElement: <ErrorRoute />,
    children: [
      {
        path: ROUTES_PATHS.LECTORS,
        element: <LectorsRoute />,
      },
      {
        path: ROUTES_PATHS.COURSES,
        element: <CoursesRoute />,
      },
      {
        path: ROUTES_PATHS.GROUPS,
        element: <GroupsRoute />,
      },
      {
        path: ROUTES_PATHS.STUDENTS,
        element: <StudentsRoute />,
      },

      {
        path: ROUTES_PATHS.STUDENTS_DETAIL,
        element: <StudentDetailRoute />,
      },
    ],
  },
]);
