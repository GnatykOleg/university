import { FC } from 'react';
import { Navigate, Outlet, useLocation } from 'react-router-dom';
import { getCurrentUser } from 'redux/auth/auth.selectors';
import { getAccessToken, ROUTES_PATHS, useAppSelector } from 'utils';

const PrivateRoute: FC = () => {
  const user = useAppSelector(getCurrentUser);

  const token = getAccessToken();

  const location = useLocation();

  const state = { from: location };

  if (user && token) return <Outlet />;

  return <Navigate to={ROUTES_PATHS.SIGN_IN} state={state} replace />;
};

export default PrivateRoute;
