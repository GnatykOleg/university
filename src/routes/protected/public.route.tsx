import { FC } from 'react';
import { Navigate, Outlet, useLocation } from 'react-router-dom';
import { getCurrentUser } from 'redux/auth/auth.selectors';
import { getAccessToken, ROUTES_PATHS, useAppSelector } from 'utils';

const PublicRoute: FC = () => {
  const user = useAppSelector(getCurrentUser);

  const token = getAccessToken();

  const location = useLocation();

  const state = { from: location };

  if (user && token)
    return <Navigate to={ROUTES_PATHS.LECTORS} state={state} replace />;

  return <Outlet />;
};

export default PublicRoute;
