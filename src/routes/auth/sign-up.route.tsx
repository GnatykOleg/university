import { AuthLayout, Button, SignUpForm } from 'components';
import { FC } from 'react';
import { useNavigate } from 'react-router-dom';
import { ROUTES_PATHS } from 'utils';

const SignUpRoute: FC = () => {
  const navigate = useNavigate();

  const goToSignIn = (): void => navigate(ROUTES_PATHS.SIGN_IN);

  return (
    <AuthLayout title="Register your account" marginBottomToChildren={32}>
      <SignUpForm marginBottom={8} />

      <Button
        variant="text"
        text="Already have an account? Sign in."
        onClick={goToSignIn}
      />
    </AuthLayout>
  );
};

export default SignUpRoute;
