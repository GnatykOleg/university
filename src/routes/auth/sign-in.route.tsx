import { AuthLayout, Button, SignInForm } from 'components';
import { FC } from 'react';
import { useNavigate } from 'react-router-dom';
import { ROUTES_PATHS } from 'utils';

const SignInRoute: FC = () => {
  const navigate = useNavigate();

  const goToSignUp = (): void => navigate(ROUTES_PATHS.SIGN_UP);

  const goToSignForgotPassword = (): void =>
    navigate(ROUTES_PATHS.FORGOT_PASSWORD);

  return (
    <AuthLayout title="Welcome" marginBottomToChildren={32}>
      <SignInForm marginBottom={8} />

      <Button
        variant="text"
        text="Forgot password?"
        onClick={goToSignForgotPassword}
        marginBottom={8}
      />

      <Button
        variant="text"
        text="Don't have an account? Sign up."
        onClick={goToSignUp}
      />
    </AuthLayout>
  );
};

export default SignInRoute;
