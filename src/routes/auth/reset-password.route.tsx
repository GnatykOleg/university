import {
  AuthDescription,
  AuthLayout,
  Button,
  ResetPasswordForm,
} from 'components';
import { FC, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { ROUTES_PATHS } from 'utils';

const successChangedPasswordDescription =
  'You can use your new password to log into your account';

const ResetPasswordRoute: FC = () => {
  const [isSuccessChangePassword, setIsSuccessChangePassword] =
    useState<boolean>(false);

  const navigate = useNavigate();

  const goToSignIn = (): void => navigate(ROUTES_PATHS.SIGN_IN);

  const marginBottomToChildren = isSuccessChangePassword ? 16 : 32;

  if (isSuccessChangePassword)
    return (
      <AuthLayout
        title="Password changed"
        marginBottomToChildren={marginBottomToChildren}
      >
        <AuthDescription
          isCentered
          text={successChangedPasswordDescription}
          marginBottom={32}
        />

        <Button
          variant="filled"
          text="Log In"
          type="button"
          onClick={goToSignIn}
        />
      </AuthLayout>
    );

  return (
    <AuthLayout
      title="Reset Your Password"
      marginBottomToChildren={marginBottomToChildren}
    >
      <ResetPasswordForm
        setIsSuccessChangePassword={setIsSuccessChangePassword}
      />
    </AuthLayout>
  );
};

export default ResetPasswordRoute;
