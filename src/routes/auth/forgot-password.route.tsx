import {
  AuthDescription,
  AuthLayout,
  Button,
  ForgotPasswordForm,
} from 'components';
import { FC, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { ROUTES_PATHS } from 'utils';

const ForgotPasswordRoute: FC = () => {
  const [isValidEmail, setIsValidEmail] = useState<boolean>(false);

  const navigate = useNavigate();

  const goToSignIn = (): void => navigate(ROUTES_PATHS.SIGN_IN);

  const authDescriptionText = isValidEmail
    ? 'You received an email with further instructions on resetting the password for the specified email'
    : "Don't worry, happens to the best of us. Enter the email address associated with your account and we'll send you a link to reset.";

  const authLayoutTitle = isValidEmail
    ? 'Check your email'
    : 'Forgot password?';

  return (
    <AuthLayout title={authLayoutTitle} marginBottomToChildren={32}>
      <AuthDescription
        text={authDescriptionText}
        marginBottom={32}
        isCentered={isValidEmail}
      />

      {!isValidEmail && (
        <ForgotPasswordForm
          marginBottom={8}
          setIsValidEmail={setIsValidEmail}
        />
      )}

      <Button variant="text" text="Cancel" onClick={goToSignIn} />
    </AuthLayout>
  );
};

export default ForgotPasswordRoute;
