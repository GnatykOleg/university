import { Error } from 'components';
import { FC } from 'react';
import { isRouteErrorResponse, useRouteError } from 'react-router-dom';

const ErrorRoute: FC = () => {
  const error = useRouteError();

  const errorMessage = isRouteErrorResponse(error)
    ? `${error.status} ${error.statusText}`
    : 'Unknown Error';

  return (
    <Error>
      <i>{errorMessage}</i>
    </Error>
  );
};

export default ErrorRoute;
