import dashboardIconsSprite from 'assets/icons/dashboard-icons-sprite.svg';
import cnBind from 'classnames/bind';
import { GoBackButton, Header, Loader, UpdateStudentForm } from 'components';
import { FC, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import { getImageSize } from 'react-image-size';
import { useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import {
  getAllStudents,
  getIsStudentsLoading,
} from 'redux/students/students.selectors';
import { useAppSelector } from 'utils';

import styles from './student-detail.module.scss';

const cnb = cnBind.bind(styles);

const StudentDetailRoute: FC = () => {
  const { id: studentId } = useParams();

  const allStudents = useAppSelector(getAllStudents);

  const foundStudent = allStudents?.find(({ id }) => id === studentId);

  const [imagePreview, setImagePreview] = useState<string | undefined | null>(
    foundStudent?.imagePath,
  );

  const [selectedFile, setSelectedFile] = useState<any>();

  const loading = useAppSelector(getIsStudentsLoading);

  const allowedExtensions = ['image/jpeg', 'image/png'];

  const maxFileSizeInBytes = 10485760;
  const minImageWidth = 400;
  const minImageHeight = 400;

  const onDrop = async (acceptedFiles: any): Promise<void> => {
    if (!allowedExtensions.includes(acceptedFiles[0].type)) {
      toast.error('File extention must be ,jpg or .png');
      return;
    }
    if (acceptedFiles[0].size > maxFileSizeInBytes) {
      toast.error('File size must be less than 10MB');
      return;
    }

    const imageUrl = URL.createObjectURL(acceptedFiles[0]);

    const dimensions = await getImageSize(imageUrl);

    if (
      dimensions.width < minImageWidth ||
      dimensions.height < minImageHeight
    ) {
      toast.error('Image resolution must be at least 400x400 pixels');

      return;
    }

    setSelectedFile(acceptedFiles[0]);

    setImagePreview(imageUrl);
  };

  const { getRootProps, getInputProps } = useDropzone({ onDrop });

  if (loading) return <Loader />;
  return (
    <>
      <Header title="Student detail" isPrimaryHeader={false} />

      <section className={cnb('section')}>
        <div className={cnb('go-back-button-wrapper')}>
          <GoBackButton />
        </div>

        <div className={cnb('section__content')}>
          <h2 className={cnb('section__title')}>Personal information</h2>
          <div className={cnb('image-wrapper')}>
            {!imagePreview && (
              <div {...getRootProps()} className={cnb('drag-and-drop')}>
                <input {...getInputProps()} />

                <svg
                  className={cnb('drag-and-drop__icon')}
                  aria-label="drag and drop icon"
                >
                  <use href={dashboardIconsSprite + '#drag-and-drop'}></use>
                </svg>
              </div>
            )}

            {imagePreview && (
              <img
                className={cnb('image')}
                src={imagePreview}
                alt="person avatar"
                width={168}
                height={168}
                loading="lazy"
              />
            )}

            <div className={cnb('image__content')}>
              <button
                onClick={(): void => setImagePreview('')}
                disabled={!imagePreview}
                className={cnb('image__button', {
                  'image__button--disable': !imagePreview,
                })}
                type="button"
              >
                Replace
              </button>

              {!imagePreview && (
                <p className={cnb('image__description')}>No file choosen</p>
              )}

              <p className={cnb('image__description')}>
                Must be a .jpg or .png file smaller than 10MB and at least 400px
                by 400px.
              </p>
            </div>
          </div>

          <UpdateStudentForm
            foundStudent={foundStudent}
            selectedFile={selectedFile}
          />
        </div>
      </section>
    </>
  );
};

export default StudentDetailRoute;
