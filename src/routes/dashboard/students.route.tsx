import { nanoid } from '@reduxjs/toolkit';
import missingAvatar from 'assets/images/missing-avatar.png';
import {
  CreateStudentForm,
  DashboardActions,
  DashboardLayout,
  Loader,
  Modal,
  Table,
  TableColumn,
  TableColumnButton,
  TableRow,
} from 'components';
import { FC, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import {
  getFindByInputValue,
  getFindBySelectValue,
  getPage,
  getSortByField,
  getSortByOrder,
} from 'redux/queries/queries.selectors';
import { clearQueries } from 'redux/queries/queries.slice';
import * as studentsOperations from 'redux/students/students.operations';
import {
  getAllStudents,
  getIsStudentsLoading,
  getStudentsTotalPages,
} from 'redux/students/students.selectors';
import { useAppDispatch, useAppSelector, useToggle } from 'utils';

const StudentsRoute: FC = () => {
  const dispatch = useAppDispatch();

  const sortByOrder = useAppSelector(getSortByOrder);

  const sortByField = useAppSelector(getSortByField);

  const findBySelectValue = useAppSelector(getFindBySelectValue);

  const findByInputValue = useAppSelector(getFindByInputValue);

  const page = useAppSelector(getPage);

  const navigate = useNavigate();

  useEffect(() => {
    dispatch(
      studentsOperations.findAllStudents({
        sortByField,
        sortByOrder,
        page,
        findBySelectValue,
        findByInputValue,
      }),
    );
  }, [
    dispatch,
    page,
    sortByField,
    sortByOrder,
    findBySelectValue,
    findByInputValue,
  ]);

  const { state: isModalOpen, toggle: toggleModal } = useToggle();

  const findByStudentsSelectOptions = [
    { value: 'course_name', label: 'Course name' },
    { value: 'group_name', label: 'Group name' },
    { value: 'surname', label: 'Surname' },
    { value: 'name', label: 'Name' },
  ];

  const tableHeaderColumns = [
    '',
    'Name',
    'Surname',
    'Email',
    'Age',
    'Courses',
    'Group',
  ];

  const gridTemplateColumns = '84px 15% 15% 20% 5% 15% 15% 1fr';

  const allStudents = useAppSelector(getAllStudents);

  const loading = useAppSelector(getIsStudentsLoading);

  const totalStudentsPages = useAppSelector(getStudentsTotalPages);

  const onEditStudentButtonClick = (id: string): void => {
    dispatch(clearQueries());
    navigate(`/students/${id}`);
  };

  const modalTitle = 'Add new student';

  if (loading) return <Loader />;

  return (
    <DashboardLayout title="Students" resourseTotalPages={totalStudentsPages}>
      <Modal
        isModalOpen={isModalOpen}
        toggleModal={toggleModal}
        title={modalTitle}
      >
        <CreateStudentForm />
      </Modal>

      <DashboardActions
        findByEntityOptions={findByStudentsSelectOptions}
        whatEntityAddName="student"
        onAddNewEntityButtonClick={toggleModal}
      />

      {allStudents.length ? (
        <Table
          headerColumns={tableHeaderColumns}
          gridTemplateColumns={gridTemplateColumns}
        >
          {allStudents.map(
            ({ id, imagePath, age, courses, email, group, name, surname }) => {
              const listOfCoursesAsString = courses
                .map((course) => course.name)
                .join(', ');

              return (
                <TableRow
                  key={nanoid()}
                  gridTemplateColumns={gridTemplateColumns}
                >
                  <img
                    style={{ borderRadius: 40 }}
                    src={imagePath ? imagePath : missingAvatar}
                    alt="student avatar"
                    width={40}
                    height={40}
                  />

                  <TableColumn text={name} />
                  <TableColumn text={surname} />
                  <TableColumn text={email} />

                  <TableColumn text={age.toString()} />
                  <TableColumn text={listOfCoursesAsString} />

                  <TableColumn text={group?.name} />

                  <TableColumnButton
                    onEditEntityButtonClick={onEditStudentButtonClick}
                    id={id}
                  />
                </TableRow>
              );
            },
          )}
        </Table>
      ) : null}
    </DashboardLayout>
  );
};

export default StudentsRoute;
