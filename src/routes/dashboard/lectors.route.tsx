import { nanoid } from '@reduxjs/toolkit';
import {
  DashboardActions,
  DashboardLayout,
  LectorForm,
  Loader,
  Modal,
  Table,
  TableColumn,
  TableColumnButton,
  TableRow,
} from 'components';
import { FC, useEffect, useState } from 'react';
import * as lectorsOperations from 'redux/lectors/lectors.operations';
import {
  getAllLectors,
  getIsLectorsLoading,
  getLectorsTotalPages,
} from 'redux/lectors/lectors.selectors';
import {
  getFindByInputValue,
  getFindBySelectValue,
  getPage,
  getSortByField,
  getSortByOrder,
} from 'redux/queries/queries.selectors';
import { useAppDispatch, useAppSelector, useToggle } from 'utils';

const LectorsRoute: FC = () => {
  const [isEditModal, setIsEditModal] = useState<boolean>(false);

  const [lectorId, setLectorId] = useState<string>();

  const dispatch = useAppDispatch();

  const sortByOrder = useAppSelector(getSortByOrder);

  const sortByField = useAppSelector(getSortByField);

  const findBySelectValue = useAppSelector(getFindBySelectValue);

  const findByInputValue = useAppSelector(getFindByInputValue);

  const page = useAppSelector(getPage);

  useEffect(() => {
    dispatch(
      lectorsOperations.findAllLectors({
        sortByField,
        sortByOrder,
        page,
        findBySelectValue,
        findByInputValue,
      }),
    );
  }, [
    dispatch,
    page,
    sortByField,
    sortByOrder,
    findBySelectValue,
    findByInputValue,
  ]);

  const { state: isModalOpen, toggle: toggleModal } = useToggle();

  const findByLectorSelectOptions = [
    { value: 'name', label: 'Name' },
    { value: 'surname', label: 'Surname' },
  ];

  const tableHeaderColumns = ['Name', 'Surname', 'Email'];

  const gridTemplateColumns = '25% 25% 25% 1fr';

  const allLectors = useAppSelector(getAllLectors);

  const loading = useAppSelector(getIsLectorsLoading);

  const totalLectorsPages = useAppSelector(getLectorsTotalPages);

  const onAddNewLectorButtonClick = (): void => {
    toggleModal();
    setIsEditModal(false);
  };

  const onEditLectorButtonClick = (id: string): void => {
    toggleModal();
    setIsEditModal(true);
    setLectorId(id);
  };

  const modalTitle = isEditModal ? 'Edit lector' : 'Add new lector';

  if (loading) return <Loader />;

  return (
    <DashboardLayout title="Lectors" resourseTotalPages={totalLectorsPages}>
      <Modal
        isModalOpen={isModalOpen}
        toggleModal={toggleModal}
        title={modalTitle}
      >
        <LectorForm isEditMode={isEditModal} lectorId={lectorId} />
      </Modal>

      <DashboardActions
        findByEntityOptions={findByLectorSelectOptions}
        whatEntityAddName="lector"
        onAddNewEntityButtonClick={onAddNewLectorButtonClick}
      />

      {allLectors.length ? (
        <Table
          headerColumns={tableHeaderColumns}
          gridTemplateColumns={gridTemplateColumns}
        >
          {allLectors.map(({ email, name, surname, id }) => (
            <TableRow key={nanoid()} gridTemplateColumns={gridTemplateColumns}>
              <TableColumn text={name} />
              <TableColumn text={surname} />
              <TableColumn text={email} />

              <TableColumnButton
                onEditEntityButtonClick={onEditLectorButtonClick}
                id={id}
              />
            </TableRow>
          ))}
        </Table>
      ) : null}
    </DashboardLayout>
  );
};

export default LectorsRoute;
