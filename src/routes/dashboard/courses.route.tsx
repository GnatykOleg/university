import { nanoid } from '@reduxjs/toolkit';
import {
  CourseForm,
  DashboardActions,
  DashboardLayout,
  Loader,
  Modal,
  Table,
  TableColumn,
  TableColumnButton,
  TableRow,
} from 'components';
import { FC, useEffect, useState } from 'react';
import * as coursesOperations from 'redux/courses/courses.operations';
import {
  getAllCourses,
  getCoursesTotalPages,
  getIsCoursesLoading,
} from 'redux/courses/courses.selectors';
import {
  getFindByInputValue,
  getFindBySelectValue,
  getPage,
  getSortByField,
  getSortByOrder,
} from 'redux/queries/queries.selectors';
import { useAppDispatch, useAppSelector, useToggle } from 'utils';

const CoursesRoute: FC = () => {
  const [isEditModal, setIsEditModal] = useState<boolean>(false);

  const [courseId, setCourseId] = useState<string>();

  const dispatch = useAppDispatch();

  const sortByOrder = useAppSelector(getSortByOrder);

  const sortByField = useAppSelector(getSortByField);

  const findBySelectValue = useAppSelector(getFindBySelectValue);

  const findByInputValue = useAppSelector(getFindByInputValue);

  const page = useAppSelector(getPage);

  useEffect(() => {
    dispatch(
      coursesOperations.findAllCourses({
        sortByField,
        sortByOrder,
        page,
        findBySelectValue,
        findByInputValue,
      }),
    );
  }, [
    dispatch,
    page,
    sortByField,
    sortByOrder,
    findBySelectValue,
    findByInputValue,
  ]);

  const { state: isModalOpen, toggle: toggleModal } = useToggle();

  const findByCourseSelectOptions = [{ value: 'name', label: 'Name' }];

  const tableHeaderColumns = ['Name', 'Description', 'Hours', 'Students count'];

  const gridTemplateColumns = '15% 30% 15% 15% 1fr';

  const allCourses = useAppSelector(getAllCourses);

  const loading = useAppSelector(getIsCoursesLoading);

  const totalCoursesPages = useAppSelector(getCoursesTotalPages);

  const onAddNewCourseButtonClick = (): void => {
    toggleModal();
    setIsEditModal(false);
  };

  const onEditCourseButtonClick = (id: string): void => {
    toggleModal();
    setIsEditModal(true);
    setCourseId(id);
  };

  const modalTitle = isEditModal ? 'Edit course' : 'Add new course';

  if (loading) return <Loader />;

  return (
    <DashboardLayout title="Courses" resourseTotalPages={totalCoursesPages}>
      <Modal
        isModalOpen={isModalOpen}
        toggleModal={toggleModal}
        title={modalTitle}
      >
        <CourseForm isEditMode={isEditModal} courseId={courseId} />
      </Modal>

      <DashboardActions
        findByEntityOptions={findByCourseSelectOptions}
        whatEntityAddName="course"
        onAddNewEntityButtonClick={onAddNewCourseButtonClick}
      />

      {allCourses.length ? (
        <Table
          headerColumns={tableHeaderColumns}
          gridTemplateColumns={gridTemplateColumns}
        >
          {allCourses.map(({ name, id, description, hours, students }) => (
            <TableRow key={nanoid()} gridTemplateColumns={gridTemplateColumns}>
              <TableColumn text={name} />
              <TableColumn text={description} />
              <TableColumn text={hours.toString()} />
              <TableColumn text={students.length.toString()} />

              <TableColumnButton
                onEditEntityButtonClick={onEditCourseButtonClick}
                id={id}
              />
            </TableRow>
          ))}
        </Table>
      ) : null}
    </DashboardLayout>
  );
};

export default CoursesRoute;
