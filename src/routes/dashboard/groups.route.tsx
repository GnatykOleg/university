import { nanoid } from '@reduxjs/toolkit';
import {
  DashboardActions,
  DashboardLayout,
  GroupForm,
  Loader,
  Modal,
  Table,
  TableColumn,
  TableColumnButton,
  TableRow,
} from 'components';
import { FC, useEffect, useState } from 'react';
import * as groupsOperations from 'redux/groups/groups.operations';
import {
  getAllGroups,
  getGroupsTotalPages,
  getIsGroupsLoading,
} from 'redux/groups/groups.selectors';
import {
  getFindByInputValue,
  getFindBySelectValue,
  getPage,
  getSortByField,
  getSortByOrder,
} from 'redux/queries/queries.selectors';
import { useAppDispatch, useAppSelector, useToggle } from 'utils';

const GroupsRoute: FC = () => {
  const [isEditModal, setIsEditModal] = useState<boolean>(false);

  const [groupId, setGroupId] = useState<string>();

  const dispatch = useAppDispatch();

  const sortByOrder = useAppSelector(getSortByOrder);

  const sortByField = useAppSelector(getSortByField);

  const findBySelectValue = useAppSelector(getFindBySelectValue);

  const findByInputValue = useAppSelector(getFindByInputValue);

  const page = useAppSelector(getPage);

  useEffect(() => {
    dispatch(
      groupsOperations.findAllGroups({
        sortByField,
        sortByOrder,
        page,
        findBySelectValue,
        findByInputValue,
      }),
    );
  }, [
    dispatch,
    page,
    sortByField,
    sortByOrder,
    findBySelectValue,
    findByInputValue,
  ]);

  const { state: isModalOpen, toggle: toggleModal } = useToggle();

  const findByGroupSelectOptions = [{ value: 'name', label: 'Name' }];

  const tableHeaderColumns = ['Name'];

  const gridTemplateColumns = '25% 1fr';

  const allGroups = useAppSelector(getAllGroups);

  const loading = useAppSelector(getIsGroupsLoading);

  const totalGroupsPages = useAppSelector(getGroupsTotalPages);

  const onAddNewGroupButtonClick = (): void => {
    toggleModal();
    setIsEditModal(false);
  };

  const onEditGroupButtonClick = (id: string): void => {
    toggleModal();
    setIsEditModal(true);
    setGroupId(id);
  };

  const modalTitle = isEditModal ? 'Edit group' : 'Add new group';

  if (loading) return <Loader />;

  return (
    <DashboardLayout title="Groups" resourseTotalPages={totalGroupsPages}>
      <Modal
        isModalOpen={isModalOpen}
        toggleModal={toggleModal}
        title={modalTitle}
      >
        <GroupForm isEditMode={isEditModal} groupId={groupId} />
      </Modal>

      <DashboardActions
        findByEntityOptions={findByGroupSelectOptions}
        whatEntityAddName="group"
        onAddNewEntityButtonClick={onAddNewGroupButtonClick}
      />

      {allGroups.length ? (
        <Table
          headerColumns={tableHeaderColumns}
          gridTemplateColumns={gridTemplateColumns}
        >
          {allGroups.map(({ name, id }) => (
            <TableRow key={nanoid()} gridTemplateColumns={gridTemplateColumns}>
              <TableColumn text={name} />

              <TableColumnButton
                onEditEntityButtonClick={onEditGroupButtonClick}
                id={id}
              />
            </TableRow>
          ))}
        </Table>
      ) : null}
    </DashboardLayout>
  );
};

export default GroupsRoute;
