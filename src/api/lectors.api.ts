import { AxiosResponse } from 'axios';
import { IFindAllEntitiesResponse } from 'types/api/common/find-all-entities.response.interface';
import { IUpdateEntityRequest } from 'types/api/common/update-entity.request.interface';
import { ICreateLectorRequest } from 'types/api/lectors/create-lector.request.interface';
import { ILector } from 'types/api/lectors/lector.interface';
import { IFindAllQueries } from 'types/api/queries/find-all-queries.type';
// import { IUpdateLectorRequest } from 'types/api/lectors/update-lector.request.interface';
import { PAGINATION, privateAxiosInstance } from 'utils';

export const findAllLectors = async ({
  sortByField,
  sortByOrder,
  page,
  findByInputValue,
  findBySelectValue,
}: IFindAllQueries): Promise<
  AxiosResponse<IFindAllEntitiesResponse<ILector>>
> => {
  let url = `/lectors?page=${page}&per_page=${PAGINATION.PER_PAGE}`;

  if (sortByField && sortByOrder)
    url += `&sort_field=${sortByField}&sort_order=${sortByOrder}`;

  if (findByInputValue && findBySelectValue)
    url += `&${findBySelectValue}=${findByInputValue}`;

  return privateAxiosInstance.get(url);
};

export const createLector = async (
  payload: ICreateLectorRequest,
): Promise<AxiosResponse<ILector>> =>
  privateAxiosInstance.post('/lectors', payload);

export const updateLector = async ({
  id,
  payload,
}: IUpdateEntityRequest<ICreateLectorRequest>): Promise<
  AxiosResponse<ILector>
> => privateAxiosInstance.patch(`/lectors/${id}`, payload);
