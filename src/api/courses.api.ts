import { AxiosResponse } from 'axios';
import { IFindAllEntitiesResponse } from 'types/api/common/find-all-entities.response.interface';
import { IUpdateEntityRequest } from 'types/api/common/update-entity.request.interface';
import { ICourse } from 'types/api/courses/course.interface';
import { ICreateCourseRequest } from 'types/api/courses/create-course.request.interface';
import { IFindAllQueries } from 'types/api/queries/find-all-queries.type';
import { PAGINATION, privateAxiosInstance } from 'utils';

export const findAllCourses = async ({
  sortByField,
  sortByOrder,
  page,
  findByInputValue,
  findBySelectValue,
}: IFindAllQueries): Promise<
  AxiosResponse<IFindAllEntitiesResponse<ICourse>>
> => {
  let url = `/courses?page=${page}&per_page=${PAGINATION.PER_PAGE}`;

  if (sortByField && sortByOrder)
    url += `&sort_field=${sortByField}&sort_order=${sortByOrder}`;

  if (findByInputValue && findBySelectValue)
    url += `&${findBySelectValue}=${findByInputValue}`;

  return privateAxiosInstance.get(url);
};

export const createCourse = async (
  payload: ICreateCourseRequest,
): Promise<AxiosResponse<ICourse>> =>
  privateAxiosInstance.post('/courses', payload);

export const updateCourse = async ({
  id,
  payload,
}: IUpdateEntityRequest<ICreateCourseRequest>): Promise<
  AxiosResponse<ICourse>
> => privateAxiosInstance.patch(`/courses/${id}`, payload);
