import { AxiosResponse } from 'axios';
import { IFindAllEntitiesResponse } from 'types/api/common/find-all-entities.response.interface';
import { IUpdateEntityRequest } from 'types/api/common/update-entity.request.interface';
import { ICreateGroupRequest } from 'types/api/groups/create-group.request.interface';
import { IGroup } from 'types/api/groups/group.interface';
import { IFindAllQueries } from 'types/api/queries/find-all-queries.type';
import { PAGINATION, privateAxiosInstance } from 'utils';

export const findAllGroups = async ({
  sortByField,
  sortByOrder,
  page,
  findByInputValue,
  findBySelectValue,
}: IFindAllQueries): Promise<
  AxiosResponse<IFindAllEntitiesResponse<IGroup>>
> => {
  let url = `/groups?page=${page}&per_page=${PAGINATION.PER_PAGE}`;

  if (sortByField && sortByOrder)
    url += `&sort_field=${sortByField}&sort_order=${sortByOrder}`;

  if (findByInputValue && findBySelectValue)
    url += `&${findBySelectValue}=${findByInputValue}`;

  return privateAxiosInstance.get(url);
};

export const createGroup = async (
  payload: ICreateGroupRequest,
): Promise<AxiosResponse<IGroup>> =>
  privateAxiosInstance.post('/groups', payload);

export const updateGroup = async ({
  id,
  payload,
}: IUpdateEntityRequest<ICreateGroupRequest>): Promise<AxiosResponse<IGroup>> =>
  privateAxiosInstance.patch(`/groups/${id}`, payload);
