import { AxiosResponse } from 'axios';
import { IAccessTokenResponse } from 'types/api/auth/access-token.response.interface';
import { IResetPasswordRequest } from 'types/api/auth/reset-password.request.interface';
import { IResetPasswordWithTokenRequest } from 'types/api/auth/reset-password-with-token.request.interface';
import { ISignRequest } from 'types/api/auth/sign.request.interface';
import { IUser } from 'types/api/auth/user.interface';
import { privateAxiosInstance, publicAxiosInstance } from 'utils';

export const signUp = async (
  payload: ISignRequest,
): Promise<AxiosResponse<undefined>> =>
  publicAxiosInstance.post('/auth/sign-up', payload);

export const signIn = async (
  payload: ISignRequest,
): Promise<AxiosResponse<IAccessTokenResponse>> =>
  publicAxiosInstance.post('/auth/sign-in', payload);

export const resetPasswordRequest = async (
  payload: IResetPasswordRequest,
): Promise<AxiosResponse<undefined>> =>
  publicAxiosInstance.post('/auth/reset-password-request', payload);

export const resetPassword = async (
  payload: IResetPasswordWithTokenRequest,
): Promise<AxiosResponse<undefined>> =>
  publicAxiosInstance.post('/auth/reset-password', payload);

export const getUser = async (): Promise<AxiosResponse<IUser>> =>
  privateAxiosInstance.get('/auth/me');
