import { AxiosResponse } from 'axios';
import { IFindAllEntitiesResponse } from 'types/api/common/find-all-entities.response.interface';
import { IUpdateEntityRequest } from 'types/api/common/update-entity.request.interface';
import { IFindAllQueries } from 'types/api/queries/find-all-queries.type';
import { ICreateStudentRequest } from 'types/api/students/create-student.request.interface';
import { IStudent } from 'types/api/students/student.interface';
import { PAGINATION, privateAxiosInstance } from 'utils';

export const findAllStudents = async ({
  sortByField,
  sortByOrder,
  page,
  findByInputValue,
  findBySelectValue,
}: IFindAllQueries): Promise<
  AxiosResponse<IFindAllEntitiesResponse<IStudent>>
> => {
  let url = `/students?page=${page}&per_page=${PAGINATION.PER_PAGE}`;

  if (sortByField && sortByOrder)
    url += `&sort_field=${sortByField}&sort_order=${sortByOrder}`;

  if (findByInputValue && findBySelectValue)
    url += `&${findBySelectValue}=${findByInputValue}`;

  return privateAxiosInstance.get(url);
};

export const createStudent = async (
  payload: ICreateStudentRequest,
): Promise<AxiosResponse<IStudent>> =>
  privateAxiosInstance.post('/students', payload);

export const updateStudent = async ({
  id,
  payload,
}: IUpdateEntityRequest<ICreateStudentRequest>): Promise<
  AxiosResponse<IStudent>
> => privateAxiosInstance.patch(`/students/${id}`, payload);
