import 'react-toastify/dist/ReactToastify.css';

import { FC, useEffect } from 'react';
import { RouterProvider } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import * as authOperations from 'redux/auth/auth.operations';
import { useAppDispatch } from 'utils';

import { Loader } from '../components';
import { router } from '../routes/root';

const App: FC = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(authOperations.getUser());
  }, [dispatch]);

  return (
    <>
      <RouterProvider fallbackElement={<Loader />} router={router} />
      <ToastContainer />
    </>
  );
};

export default App;
