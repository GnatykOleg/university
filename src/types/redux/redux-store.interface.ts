import { IAuthInitialState } from 'redux/auth/auth.slice';
import { ICoursesInitialState } from 'redux/courses/courses.slice';
import { IGroupsInitialState } from 'redux/groups/groups.slice';
import { ILectorsInitialState } from 'redux/lectors/lectors.slice';
import { IQueriesInitialState } from 'redux/queries/queries.slice';
import { IStudentsInitialState } from 'redux/students/students.slice';
import { REDUX_SLICE_NAMES } from 'utils';

export interface IStore {
  [REDUX_SLICE_NAMES.AUTH]: IAuthInitialState;
  [REDUX_SLICE_NAMES.LECTORS]: ILectorsInitialState;
  [REDUX_SLICE_NAMES.GROUPS]: IGroupsInitialState;
  [REDUX_SLICE_NAMES.COURSES]: ICoursesInitialState;
  [REDUX_SLICE_NAMES.STUDENTS]: IStudentsInitialState;
  [REDUX_SLICE_NAMES.QUERIES]: IQueriesInitialState;
}
