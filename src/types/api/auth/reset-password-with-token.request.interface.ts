export interface IResetPasswordWithTokenRequest {
  resetToken: string;
  newPassword: string;
}
