export interface IAccessTokenResponse {
  accessToken: string;
}
