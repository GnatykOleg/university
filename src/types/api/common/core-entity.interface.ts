export interface ICoreEntity {
  id: string;
  createdAt: string;
  updatedAt: string;
}
