export interface IFindAllEntitiesResponse<D> {
  data: D[] | [];
  total: number;
}
