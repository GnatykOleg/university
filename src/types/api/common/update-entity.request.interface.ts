export interface IUpdateEntityRequest<CreateInterface> {
  id: string;
  payload: Partial<CreateInterface>;
}
