export interface ICreateCourseRequest {
  name: string;
  description: string;
  hours: number;
}
