import { ICoreEntity } from 'types/api/common/core-entity.interface';

import { IStudent } from '../students/student.interface';

export interface ICourse extends ICoreEntity {
  name: string;
  description: string;
  hours: number;
  students: IStudent[] | [];
}
