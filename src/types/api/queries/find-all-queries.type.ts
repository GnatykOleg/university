import { FindByInputValueType } from './find-by-input-value.type';
import { FindBySelectValueType } from './find-by-select-value.type';
import { SortByFieldType } from './sort-by-filed.type';
import { SortByOrderType } from './sort-by-order.type';

export interface IFindAllQueries {
  sortByField?: SortByFieldType;
  sortByOrder?: SortByOrderType;
  page?: number;
  findByInputValue?: FindByInputValueType;
  findBySelectValue?: FindBySelectValueType;
}
