export interface ICreateStudentRequest {
  name: string;
  surname: string;
  email: string;
  age: number;
  courseId?: string | undefined;
  groupId?: string | undefined;
}
