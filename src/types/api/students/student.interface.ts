import { ICoreEntity } from '../common/core-entity.interface';
import { ICourse } from '../courses/course.interface';
import { IGroup } from '../groups/group.interface';

export interface IStudent extends ICoreEntity {
  name: string;
  surname: string;
  email: string;
  age: number;
  imagePath: string;
  group: IGroup;
  courses: Omit<ICourse[], 'students'> | [];
}
