import { ICoreEntity } from 'types/api/common/core-entity.interface';

export interface IGroup extends ICoreEntity {
  name: string;
}
