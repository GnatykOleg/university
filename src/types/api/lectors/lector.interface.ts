import { ICoreEntity } from 'types/api/common/core-entity.interface';

export interface ILector extends ICoreEntity {
  name: string | null;
  surname: string | null;
  email: string;
}
