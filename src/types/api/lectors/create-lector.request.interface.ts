export interface ICreateLectorRequest {
  name?: string | undefined;
  surname?: string | undefined;
  email: string;
  password: string;
}
