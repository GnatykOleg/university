import { joiResolver } from '@hookform/resolvers/joi';
import * as authApi from 'api/auth.api';
import { BaseForm, Button, Checkbox, Input } from 'components';
import Joi from 'joi';
import { Dispatch, FC, SetStateAction } from 'react';
import { useForm } from 'react-hook-form';
import { Navigate, useLocation, useSearchParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import {
  FORM_PLACEHOLDERS,
  generateAxiosErrorMessage,
  generateConfirmPasswordField,
  generatePasswordField,
  ROUTES_PATHS,
  useTogglePasswordVisibility,
} from 'utils';

interface IResetPasswordFormProps {
  marginBottom?: number | undefined;
  setIsSuccessChangePassword: Dispatch<SetStateAction<boolean>>;
}

interface IResetPasswordFormData {
  newPassword: string;
  confirmPassword: string;
}

const resetPasswordSchema = Joi.object({
  newPassword: generatePasswordField({
    passwordName: 'newPassword',
    optional: false,
  }),
  confirmPassword: generateConfirmPasswordField({
    passwordToMatchName: 'newPassword',
  }),
});

const ResetPasswordForm: FC<IResetPasswordFormProps> = ({
  marginBottom,
  setIsSuccessChangePassword,
}) => {
  const { inputType, passwordVisibilityToggle } = useTogglePasswordVisibility();

  const [searchParams] = useSearchParams();

  const location = useLocation();

  const state = { from: location };

  const resetToken = searchParams.get('token');

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<IResetPasswordFormData>({
    resolver: joiResolver(resetPasswordSchema),
  });

  const onSubmit = async ({
    newPassword,
  }: IResetPasswordFormData): Promise<void> => {
    try {
      if (resetToken) {
        await authApi.resetPassword({ newPassword, resetToken });

        setIsSuccessChangePassword(true);

        reset();

        toast.success('Successfully reset password');
      }
    } catch (error) {
      const errorMessage = generateAxiosErrorMessage(error);

      toast.error(errorMessage);
    }
  };

  if (!resetToken)
    return <Navigate to={ROUTES_PATHS.SIGN_IN} state={state} replace />;

  return (
    <BaseForm marginBottom={marginBottom} onSubmit={handleSubmit(onSubmit)}>
      <Input
        type={inputType}
        labelText="New Password"
        placeholder={FORM_PLACEHOLDERS.PASSWORD}
        register={register('newPassword')}
        errors={errors}
      />

      <Input
        type={inputType}
        labelText="Confirm Password"
        placeholder={FORM_PLACEHOLDERS.PASSWORD}
        register={register('confirmPassword')}
        errors={errors}
      />

      <Checkbox labelText="Show Password" onChange={passwordVisibilityToggle} />

      <Button variant="filled" text="Reset" type="submit" />
    </BaseForm>
  );
};

export default ResetPasswordForm;
