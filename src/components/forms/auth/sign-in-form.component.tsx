import { joiResolver } from '@hookform/resolvers/joi';
import * as authApi from 'api/auth.api';
import { BaseForm, Button, Checkbox, Input } from 'components';
import Joi from 'joi';
import { FC } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import * as authOperations from 'redux/auth/auth.operations';
import {
  FORM_PLACEHOLDERS,
  generateAxiosErrorMessage,
  generateEmailField,
  generatePasswordField,
  ROUTES_PATHS,
  saveAccessToken,
  useAppDispatch,
  useTogglePasswordVisibility,
} from 'utils';

interface ISignInFormProps {
  marginBottom?: number | undefined;
}

interface ISignInFormData {
  email: string;
  password: string;
}

const signInSchema = Joi.object({
  email: generateEmailField({ optional: false }),
  password: generatePasswordField({
    passwordName: 'password',
    optional: false,
  }),
});

const SignInForm: FC<ISignInFormProps> = ({ marginBottom }) => {
  const { inputType, passwordVisibilityToggle } = useTogglePasswordVisibility();

  const navigate = useNavigate();

  const dispatch = useAppDispatch();

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<ISignInFormData>({ resolver: joiResolver(signInSchema) });

  const onSubmit = async (signInFormData: ISignInFormData): Promise<void> => {
    try {
      const { email, password } = signInFormData;

      const { data } = await authApi.signIn({ email, password });

      saveAccessToken(data.accessToken);

      await dispatch(authOperations.getUser()).unwrap();

      reset();

      toast.success('Successfully sign in');

      navigate(ROUTES_PATHS.LECTORS);
    } catch (error) {
      const errorMessage = generateAxiosErrorMessage(error);

      toast.error(errorMessage);
    }
  };

  return (
    <BaseForm marginBottom={marginBottom} onSubmit={handleSubmit(onSubmit)}>
      <Input
        type="email"
        labelText="Email"
        placeholder={FORM_PLACEHOLDERS.EMAIL}
        register={register('email')}
        errors={errors}
      />

      <Input
        type={inputType}
        labelText="Password"
        placeholder={FORM_PLACEHOLDERS.PASSWORD}
        register={register('password')}
        errors={errors}
      />

      <Checkbox labelText="Show Password" onChange={passwordVisibilityToggle} />

      <Button variant="filled" text="Login" type="submit" />
    </BaseForm>
  );
};

export default SignInForm;
