import { joiResolver } from '@hookform/resolvers/joi';
import * as authApi from 'api/auth.api';
import { BaseForm, Button, Checkbox, Input } from 'components';
import Joi from 'joi';
import { FC } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router';
import { toast } from 'react-toastify';
import {
  FORM_PLACEHOLDERS,
  generateAxiosErrorMessage,
  generateConfirmPasswordField,
  generateEmailField,
  generatePasswordField,
  ROUTES_PATHS,
  useTogglePasswordVisibility,
} from 'utils';

interface ISignUpFormProps {
  marginBottom?: number | undefined;
}

interface ISignUpFormData {
  email: string;
  password: string;
  confirmPassword: string;
}

const signUpSchema = Joi.object({
  email: generateEmailField({ optional: false }),
  password: generatePasswordField({
    passwordName: 'password',
    optional: false,
  }),
  confirmPassword: generateConfirmPasswordField({
    passwordToMatchName: 'password',
  }),
});

const SignUpForm: FC<ISignUpFormProps> = ({ marginBottom }) => {
  const { inputType, passwordVisibilityToggle } = useTogglePasswordVisibility();

  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<ISignUpFormData>({
    resolver: joiResolver(signUpSchema),
  });

  const onSubmit = async (signUpFormData: ISignUpFormData): Promise<void> => {
    try {
      const { email, password } = signUpFormData;

      await authApi.signUp({ email, password });

      reset();

      toast.success('Successful sign up!');

      navigate(ROUTES_PATHS.SIGN_IN);
    } catch (error) {
      const errorMessage = generateAxiosErrorMessage(error);

      toast.error(errorMessage);
    }
  };

  return (
    <BaseForm marginBottom={marginBottom} onSubmit={handleSubmit(onSubmit)}>
      <Input
        type="email"
        labelText="Email"
        placeholder={FORM_PLACEHOLDERS.EMAIL}
        register={register('email')}
        errors={errors}
      />
      <Input
        type={inputType}
        labelText="Password"
        placeholder={FORM_PLACEHOLDERS.PASSWORD}
        register={register('password')}
        errors={errors}
      />
      <Input
        type={inputType}
        labelText="Confirm Password"
        placeholder={FORM_PLACEHOLDERS.PASSWORD}
        register={register('confirmPassword')}
        errors={errors}
      />

      <Checkbox labelText="Show Password" onChange={passwordVisibilityToggle} />

      <Button variant="filled" text="Register" type="submit" />
    </BaseForm>
  );
};

export default SignUpForm;
