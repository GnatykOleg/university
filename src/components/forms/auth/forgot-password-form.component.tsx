import { joiResolver } from '@hookform/resolvers/joi';
import * as authApi from 'api/auth.api';
import { BaseForm, Button, Input } from 'components';
import Joi from 'joi';
import { Dispatch, FC, SetStateAction } from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import {
  FORM_PLACEHOLDERS,
  generateAxiosErrorMessage,
  generateEmailField,
} from 'utils';

interface IForgotPasswordFormProps {
  marginBottom?: number | undefined;
  setIsValidEmail: Dispatch<SetStateAction<boolean>>;
}

interface IForgotPasswordFormData {
  email: string;
}

const forgotPasswordSchema = Joi.object({
  email: generateEmailField({ optional: false }),
});

const ForgotPasswordForm: FC<IForgotPasswordFormProps> = ({
  marginBottom,
  setIsValidEmail,
}) => {
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<IForgotPasswordFormData>({
    resolver: joiResolver(forgotPasswordSchema),
  });

  const onSubmit = async (
    forgotPasswordFormData: IForgotPasswordFormData,
  ): Promise<void> => {
    try {
      const { email } = forgotPasswordFormData;

      await authApi.resetPasswordRequest({ email });

      setIsValidEmail(true);

      reset();

      toast.success('Check your email');
    } catch (error) {
      const errorMessage = generateAxiosErrorMessage(error);

      toast.error(errorMessage);
    }
  };

  return (
    <BaseForm marginBottom={marginBottom} onSubmit={handleSubmit(onSubmit)}>
      <Input
        type="email"
        labelText="Email"
        placeholder={FORM_PLACEHOLDERS.EMAIL}
        register={register('email')}
        errors={errors}
      />
      <Button variant="filled" text="Reset" type="submit" />
    </BaseForm>
  );
};

export default ForgotPasswordForm;
