import { joiResolver } from '@hookform/resolvers/joi';
import { BaseForm, Button, Input } from 'components';
import Joi from 'joi';
import { FC } from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import * as coursesOperations from 'redux/courses/courses.operations';
import { getAllCourses } from 'redux/courses/courses.selectors';
import {
  FORM_PLACEHOLDERS,
  generateDescriptionField,
  generateHoursField,
  generateNameField,
  useAppDispatch,
  useAppSelector,
} from 'utils';

interface ICourseFormProps {
  isEditMode: boolean;
  courseId?: string | undefined;
}

export interface ICourseFormData {
  name: string;

  description: string;

  hours: number;
}

const CourseForm: FC<ICourseFormProps> = ({ isEditMode, courseId }) => {
  const courseSchema = Joi.object({
    name: generateNameField({ optional: isEditMode }),
    description: generateDescriptionField({ optional: isEditMode }),
    hours: generateHoursField({ optional: isEditMode }),
  });

  const allCourses = useAppSelector(getAllCourses);

  const foundCourse = allCourses.find(({ id }) => id === courseId);

  const dispatch = useAppDispatch();

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<ICourseFormData>({
    resolver: joiResolver(courseSchema),
    defaultValues: {
      name: (isEditMode && foundCourse?.name) || '',
      description: (isEditMode && foundCourse?.description) || '',
      hours: (isEditMode && foundCourse?.hours) || 0,
    },
  });

  const onSubmit = async (courseFormData: ICourseFormData): Promise<void> => {
    try {
      const { name, description, hours } = courseFormData;

      if (isEditMode && courseId) {
        await dispatch(
          coursesOperations.updateCourse({
            payload: { name, description, hours },
            id: courseId,
          }),
        ).unwrap();

        toast.success('Successful update course!');

        return;
      }

      await dispatch(
        coursesOperations.createCourse({ name, description, hours }),
      ).unwrap();

      toast.success('Successful create course!');

      reset();
    } catch (error) {
      toast.error(error as string);
    }
  };

  return (
    <BaseForm onSubmit={handleSubmit(onSubmit)}>
      <Input
        type="text"
        labelText="Name"
        placeholder={FORM_PLACEHOLDERS.COURSE_NAME}
        register={register('name')}
        errors={errors}
      />

      <Input
        type="text"
        labelText="Description"
        placeholder={FORM_PLACEHOLDERS.COURSE_DESCRIPTION}
        register={register('description')}
        errors={errors}
      />

      <Input
        type="number"
        labelText="Hours"
        placeholder={FORM_PLACEHOLDERS.COURSE_DESCRIPTION}
        register={register('hours')}
        errors={errors}
      />

      <Button
        variant="filled"
        text={isEditMode ? 'Edit' : 'Create'}
        type="submit"
      />
    </BaseForm>
  );
};

export default CourseForm;
