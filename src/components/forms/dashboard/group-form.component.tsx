import { joiResolver } from '@hookform/resolvers/joi';
import { Button, Input } from 'components';
import Joi from 'joi';
import { FC } from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import * as groupsOperations from 'redux/groups/groups.operations';
import { getAllGroups } from 'redux/groups/groups.selectors';
import {
  FORM_PLACEHOLDERS,
  generateNameField,
  useAppDispatch,
  useAppSelector,
} from 'utils';

import BaseForm from '../base-form/base-form.component';

interface IGroupFormProps {
  isEditMode: boolean;
  groupId?: string | undefined;
}

export interface IGroupFormData {
  name: string;
}

const GroupForm: FC<IGroupFormProps> = ({ isEditMode, groupId }) => {
  const groupSchema = Joi.object({
    name: generateNameField({ optional: isEditMode }),
  });

  const allGroups = useAppSelector(getAllGroups);

  const foundGroup = allGroups.find(({ id }) => id === groupId);

  const dispatch = useAppDispatch();

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<IGroupFormData>({
    resolver: joiResolver(groupSchema),
    defaultValues: {
      name: (isEditMode && foundGroup?.name) || '',
    },
  });

  const onSubmit = async (groupFormData: IGroupFormData): Promise<void> => {
    try {
      const { name } = groupFormData;

      if (isEditMode && groupId) {
        await dispatch(
          groupsOperations.updateGroup({
            payload: { name },
            id: groupId,
          }),
        ).unwrap();

        toast.success('Successful update group!');

        return;
      }

      await dispatch(
        groupsOperations.createGroup({
          name,
        }),
      ).unwrap();

      toast.success('Successful create group!');

      reset();
    } catch (error) {
      toast.error(error as string);
    }
  };

  return (
    <BaseForm onSubmit={handleSubmit(onSubmit)}>
      <Input
        type="text"
        labelText="Name"
        placeholder={FORM_PLACEHOLDERS.GROUP_NAME}
        register={register('name')}
        errors={errors}
      />

      <Button
        variant="filled"
        text={isEditMode ? 'Edit' : 'Create'}
        type="submit"
      />
    </BaseForm>
  );
};

export default GroupForm;
