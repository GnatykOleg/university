import { joiResolver } from '@hookform/resolvers/joi';
import { Button, Input } from 'components';
import Joi from 'joi';
import { FC } from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import * as studentsOperations from 'redux/students/students.operations';
import {
  FORM_PLACEHOLDERS,
  generateAgeField,
  generateEmailField,
  generateNameField,
  generateSurnameField,
  useAppDispatch,
} from 'utils';

import BaseForm from '../base-form/base-form.component';

export interface ICreateStudentFormData {
  name: string;
  surname: string;
  email: string;
  age: number;
}

const CreateStudentForm: FC = () => {
  const studentSchema = Joi.object({
    name: generateNameField({ optional: false }),
    surname: generateSurnameField({ optional: false }),
    email: generateEmailField({ optional: false }),
    age: generateAgeField({ optional: false }),
  });

  const dispatch = useAppDispatch();

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<ICreateStudentFormData>({
    resolver: joiResolver(studentSchema),
  });

  const onSubmit = async (
    createStudentFormData: ICreateStudentFormData,
  ): Promise<void> => {
    try {
      const { age, email, name, surname } = createStudentFormData;

      await dispatch(
        studentsOperations.createStudent({ age, email, name, surname }),
      ).unwrap();

      toast.success('Successful create student!');

      reset();
    } catch (error) {
      toast.error(error as string);
    }
  };

  return (
    <BaseForm onSubmit={handleSubmit(onSubmit)}>
      <Input
        type="text"
        labelText="Name"
        placeholder={FORM_PLACEHOLDERS.NAME}
        register={register('name')}
        errors={errors}
      />

      <Input
        type="text"
        labelText="Surname"
        placeholder={FORM_PLACEHOLDERS.SURNAME}
        register={register('surname')}
        errors={errors}
      />

      <Input
        type="email"
        labelText="Email"
        placeholder={FORM_PLACEHOLDERS.EMAIL}
        register={register('email')}
        errors={errors}
      />

      <Input
        type="number"
        labelText="Age"
        placeholder={FORM_PLACEHOLDERS.AGE}
        register={register('age')}
        errors={errors}
      />

      <Button variant="filled" text="Create" type="submit" />
    </BaseForm>
  );
};

export default CreateStudentForm;
