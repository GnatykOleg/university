import { joiResolver } from '@hookform/resolvers/joi';
import { Button, Input } from 'components';
import Joi from 'joi';
import { FC, useEffect } from 'react';
import { Controller, useForm } from 'react-hook-form';
import Select, { StylesConfig } from 'react-select';
import { toast } from 'react-toastify';
import * as coursesOperations from 'redux/courses/courses.operations';
import { getAllCourses } from 'redux/courses/courses.selectors';
import * as groupsOperations from 'redux/groups/groups.operations';
import { getAllGroups } from 'redux/groups/groups.selectors';
import * as studentsOperations from 'redux/students/students.operations';
import { IOption } from 'types/api/queries/select-option.interface';
import { IStudent } from 'types/api/students/student.interface';
import {
  FORM_PLACEHOLDERS,
  generateAgeField,
  generateEmailField,
  generateNameField,
  generateSurnameField,
  privateAxiosInstance,
  useAppDispatch,
  useAppSelector,
} from 'utils';

import BaseForm from '../base-form/base-form.component';
import styles from './update-student-form.module.scss';

interface IUpdateStudentFormProps {
  foundStudent: IStudent | undefined;
  selectedFile: any;
}

export interface IUpdateStudentFormData {
  name?: string | undefined;
  surname?: string | undefined;
  email?: string | undefined;
  age?: number | undefined;

  selectCourse?: IOption;
  selectGroup?: IOption;
}

const updateStudentSchema = Joi.object({
  name: generateNameField({ optional: true }),
  surname: generateSurnameField({ optional: true }),
  email: generateEmailField({ optional: true }),
  age: generateAgeField({ optional: true }),

  selectCourse: Joi.object({
    label: Joi.string().optional(),
    value: Joi.string().optional(),
  }).allow(''),

  selectGroup: Joi.object({
    label: Joi.string().optional(),
    value: Joi.string().optional(),
  }).allow(''),
});

export const selectStyles: StylesConfig<IOption> = {
  container: () => ({
    position: 'relative',
    width: 376,
    height: 48,
    color: '#000',
    fontFamily: 'Lexend',
    fontSize: 15,

    lineHeight: 1.6,
  }),

  control: () => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    maxHeight: '100%',
    padding: '12px 23px 12px 16px',

    border: '1px solid #505E68',
    backgroundColor: '#FFF',
  }),

  valueContainer: () => ({
    '> input': {
      display: 'none',
    },
  }),

  singleValue: () => ({ pointerEvents: 'none' }),

  menu: () => ({
    width: '100%',
    position: 'absolute',
    top: '100%',
    zIndex: 1,
    padding: '12px 0px 12px 16px',
    backgroundColor: '#FFF',

    borderBottom: '1px solid #505E68',
    borderLeft: '1px solid #505E68',
    borderRight: '1px solid #505E68',
  }),

  menuList: () => ({}),

  option: (_, state) => ({
    transition: 'all 0.6s ease',
    cursor: 'pointer',
    color: state.isSelected ? '#3B4360' : 'rgba(59, 67, 96, 0.45)',
    ':not(:last-child)': {
      marginBottom: 8,
    },
    ':hover': {
      color: '#3B4360',
    },
  }),
  indicatorsContainer: () => ({ cursor: 'pointer' }),

  dropdownIndicator: (_, state) => ({
    display: 'flex',
    alignItems: 'center',
    transform: state.selectProps.menuIsOpen ? 'rotate(180deg)' : 'none',
  }),
};

const UpdateStudentForm: FC<IUpdateStudentFormProps> = ({
  foundStudent,
  selectedFile,
}) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(groupsOperations.findAllGroups({ page: 1 }));

    dispatch(coursesOperations.findAllCourses({ page: 1 }));
  }, [dispatch]);

  const allGroups = useAppSelector(getAllGroups);

  const allCourses = useAppSelector(getAllCourses);

  const groupsSelectOptions: IOption[] = allGroups.map(({ id, name }) => ({
    label: name,
    value: id,
  }));

  const coursesSelectOptions: IOption[] = allCourses.map(({ id, name }) => ({
    label: name,
    value: id,
  }));

  const {
    register,
    handleSubmit,
    control,

    formState: { errors },
  } = useForm<IUpdateStudentFormData>({
    resolver: joiResolver(updateStudentSchema),
    defaultValues: {
      email: foundStudent?.email,
      name: foundStudent?.name,
      surname: foundStudent?.surname,
      age: foundStudent?.age,
    },
  });

  const onSubmit = async (
    updateStudentFormData: IUpdateStudentFormData,
  ): Promise<void> => {
    const { age, email, name, surname, selectCourse, selectGroup } =
      updateStudentFormData;

    const formData = new FormData();

    formData.append('file', selectedFile);

    try {
      if (selectedFile)
        await privateAxiosInstance.patch(
          `/students/${foundStudent?.id}/image`,
          formData,
        );

      if (foundStudent?.id) {
        await dispatch(
          studentsOperations.updateStudent({
            id: foundStudent.id,
            payload: {
              age,
              email,
              name,
              surname,
              courseId: selectCourse?.value,
              groupId: selectGroup?.value,
            },
          }),
        ).unwrap();
        toast.success('Successful update student!');
      }
    } catch (error) {
      toast.error(error as string);
    }
  };
  return (
    <>
      <BaseForm onSubmit={handleSubmit(onSubmit)}>
        <Input
          type="text"
          labelText="Name"
          placeholder={FORM_PLACEHOLDERS.NAME}
          register={register('name')}
          errors={errors}
        />
        <Input
          type="text"
          labelText="Surname"
          placeholder={FORM_PLACEHOLDERS.SURNAME}
          register={register('surname')}
          errors={errors}
        />
        <Input
          type="email"
          labelText="Email"
          placeholder={FORM_PLACEHOLDERS.EMAIL}
          register={register('email')}
          errors={errors}
        />
        <Input
          type="number"
          labelText="Age"
          placeholder={FORM_PLACEHOLDERS.AGE}
          register={register('age')}
          errors={errors}
        />
        <Button variant="filled" text="Save changes" type="submit" />
      </BaseForm>

      <div className={styles.wrapper}>
        <h3 className={styles.title}>Courses and Groups</h3>
        <div className={styles.select}>
          <p className={styles.select__text}>Course</p>

          <Controller
            name="selectCourse"
            control={control}
            render={({ field }): JSX.Element => (
              <Select
                {...field}
                styles={selectStyles}
                options={coursesSelectOptions}
                isSearchable={false}
              />
            )}
          />
        </div>
        <div>
          <p className={styles.select__text}>Group</p>

          <Controller
            name="selectGroup"
            control={control}
            render={({ field }): JSX.Element => (
              <Select
                {...field}
                styles={selectStyles}
                options={groupsSelectOptions}
                isSearchable={false}
              />
            )}
          />
        </div>
      </div>
    </>
  );
};

export default UpdateStudentForm;
