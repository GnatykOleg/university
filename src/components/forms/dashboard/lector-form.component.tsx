import { joiResolver } from '@hookform/resolvers/joi';
import { Button, Checkbox, Input } from 'components';
import Joi from 'joi';
import { FC } from 'react';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import * as lectorsOperaions from 'redux/lectors/lectors.operations';
import { getAllLectors } from 'redux/lectors/lectors.selectors';
import {
  FORM_PLACEHOLDERS,
  generateEmailField,
  generateNameField,
  generatePasswordField,
  generateSurnameField,
  useAppDispatch,
  useAppSelector,
  useTogglePasswordVisibility,
} from 'utils';

import BaseForm from '../base-form/base-form.component';

interface ILectorFormProps {
  isEditMode: boolean;
  lectorId?: string | undefined;
}

export interface ILectorFormData {
  name?: string | undefined;
  surname?: string | undefined;
  email: string;
  password: string;
}

const LectorForm: FC<ILectorFormProps> = ({ isEditMode, lectorId }) => {
  const lectorSchema = Joi.object({
    name: generateNameField({ optional: true }),
    surname: generateSurnameField({ optional: true }),
    email: generateEmailField({ optional: isEditMode }),
    password: generatePasswordField({
      passwordName: 'password',
      optional: isEditMode,
    }),
  });

  const allLectors = useAppSelector(getAllLectors);

  const foundLector = allLectors.find(({ id }) => id === lectorId);

  const dispatch = useAppDispatch();

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<ILectorFormData>({
    resolver: joiResolver(lectorSchema),
    defaultValues: {
      email: (isEditMode && foundLector?.email) || '',
      name: (isEditMode && foundLector?.name) || '',
      surname: (isEditMode && foundLector?.surname) || '',
    },
  });

  const onSubmit = async (lectorFormData: ILectorFormData): Promise<void> => {
    try {
      const { email, name, password, surname } = lectorFormData;

      if (isEditMode && lectorId) {
        await dispatch(
          lectorsOperaions.updateLector({
            payload: { email, name, password, surname },
            id: lectorId,
          }),
        ).unwrap();

        toast.success('Successful update lector!');

        return;
      }

      await dispatch(
        lectorsOperaions.createLector({
          email,
          name,
          password,
          surname,
        }),
      ).unwrap();

      toast.success('Successful create lector!');

      reset();
    } catch (error) {
      toast.error(error as string);
    }
  };

  const { inputType, passwordVisibilityToggle } = useTogglePasswordVisibility();

  return (
    <BaseForm onSubmit={handleSubmit(onSubmit)}>
      <Input
        type="text"
        labelText="Name"
        placeholder={FORM_PLACEHOLDERS.NAME}
        register={register('name')}
        errors={errors}
      />

      <Input
        type="text"
        labelText="Surname"
        placeholder={FORM_PLACEHOLDERS.SURNAME}
        register={register('surname')}
        errors={errors}
      />

      <Input
        type="email"
        labelText="Email"
        placeholder={FORM_PLACEHOLDERS.EMAIL}
        register={register('email')}
        errors={errors}
      />

      <Input
        type={inputType}
        labelText="Password"
        placeholder={FORM_PLACEHOLDERS.PASSWORD}
        register={register('password')}
        errors={errors}
      />

      <Checkbox labelText="Show Password" onChange={passwordVisibilityToggle} />

      <Button
        variant="filled"
        text={isEditMode ? 'Edit' : 'Create'}
        type="submit"
      />
    </BaseForm>
  );
};

export default LectorForm;
