import { FC, FormEvent, ReactNode } from 'react';

import styles from './base-form.module.scss';

interface IBaseFormProps {
  onSubmit: (event: FormEvent<HTMLFormElement>) => void;
  marginBottom?: number | undefined;
  children: ReactNode;
}

const BaseForm: FC<IBaseFormProps> = ({ children, onSubmit, marginBottom }) => (
  <form className={styles.form} style={{ marginBottom }} onSubmit={onSubmit}>
    {children}
  </form>
);

export default BaseForm;
