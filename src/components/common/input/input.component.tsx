import cnBind from 'classnames/bind';
import { FC, HTMLInputTypeAttribute } from 'react';
import {
  FieldErrors,
  FieldValues,
  UseFormRegisterReturn,
} from 'react-hook-form';

import styles from './input.module.scss';

interface IInputProps {
  labelText: string;
  placeholder: string;
  type: HTMLInputTypeAttribute;
  register: UseFormRegisterReturn<string>;
  errors: FieldErrors<FieldValues>;
}

const cnb = cnBind.bind(styles);

const Input: FC<IInputProps> = ({
  labelText,
  placeholder,
  type,
  register,
  errors,
}) => {
  const errorMessage = errors[register.name]?.message as string;

  const ariaInvalid = errorMessage ? 'true' : 'false';

  const labelClassName = cnb('label');

  const labelTextClassName = cnb('label__text', {
    'label__text--error': errorMessage,
  });

  const labelInputClassName = cnb('label__input', {
    'label__input--error': errorMessage,
  });

  const labelErrorMessageClassName = cnb('label__error-message');

  return (
    <label className={labelClassName}>
      <span className={labelTextClassName}>{labelText}</span>

      <input
        className={labelInputClassName}
        type={type}
        placeholder={placeholder}
        autoComplete="off"
        {...register}
        aria-invalid={ariaInvalid}
      />

      {errorMessage && (
        <p role="alert" className={labelErrorMessageClassName}>
          {errorMessage}
        </p>
      )}
    </label>
  );
};

export default Input;
