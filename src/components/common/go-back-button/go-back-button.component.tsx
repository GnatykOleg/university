import dashboardIconsSprite from 'assets/icons/dashboard-icons-sprite.svg';
import { FC } from 'react';
import { useNavigate } from 'react-router-dom';

import styles from './go-back-button.module.scss';

const GoBackButton: FC = () => {
  const navigate = useNavigate();

  const onBackButtonClick = (): void => navigate(-1);

  return (
    <button onClick={onBackButtonClick} type="button" className={styles.button}>
      <svg className={styles.button__icon}>
        <use href={dashboardIconsSprite + '#back-arrow'}></use>
      </svg>
      Back
    </button>
  );
};

export default GoBackButton;
