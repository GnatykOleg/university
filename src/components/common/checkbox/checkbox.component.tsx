import cnBind from 'classnames/bind';
import { FC } from 'react';

import styles from './checkbox.module.scss';

interface ICheckboxProps {
  labelText: string;
  onChange: () => void;
}

const cnb = cnBind.bind(styles);

const Checkbox: FC<ICheckboxProps> = ({ labelText, onChange }) => (
  <label className={cnb('checkbox')}>
    <input
      onChange={onChange}
      className={cnb('checkbox__input')}
      type="checkbox"
    />

    <span className={cnb('checkbox__input--custom')} />

    <span className={cnb('checkbox__text')}>{labelText}</span>
  </label>
);

export default Checkbox;
