import cnBind from 'classnames/bind';
import { FC } from 'react';

import styles from './button.module.scss';

interface IButtonProps {
  text: string;
  variant: 'filled' | 'text';
  type?: 'button' | 'submit' | 'reset' | undefined;

  marginBottom?: number | undefined;

  onClick?: () => void;
}

const cnb = cnBind.bind(styles);

const Button: FC<IButtonProps> = ({
  type = 'button',
  text,
  marginBottom,
  variant,
  onClick,
}) => {
  const buttonClassName = cnb('button', {
    'button--text': variant === 'text',
    'button--filled': variant === 'filled',
  });

  return (
    <button
      className={buttonClassName}
      type={type}
      style={{ marginBottom }}
      onClick={onClick}
    >
      {text}
    </button>
  );
};

export default Button;
