import notFound from 'assets/images/not-found.png';
import { Button } from 'components';
import { FC, ReactNode } from 'react';
import { useNavigate } from 'react-router-dom';

import styles from './error.module.scss';

interface IErrorProps {
  children: ReactNode;
}

const Error: FC<IErrorProps> = ({ children }) => {
  const navigate = useNavigate();

  const goBack = (): void => navigate(-1);

  return (
    <div className={styles.error}>
      <h2 className={styles.error__title}>Oops!</h2>
      <img
        className={styles.error__image}
        src={notFound}
        alt="error page"
        width={500}
        loading="lazy"
      />

      <p className={styles.error__text}>
        Sorry, an unexpected error has occurred.
      </p>

      <p className={styles.error__text}>{children}</p>

      <Button variant="text" text="Go Back" onClick={goBack} />
    </div>
  );
};

export default Error;
