import dashboardIconsSprite from 'assets/icons/dashboard-icons-sprite.svg';
import { FC, ReactNode } from 'react';
import { createPortal } from 'react-dom';

import styles from './modal.module.scss';

// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
const modalRoot = document.querySelector('#modal')!;

export interface IModalProps {
  children: ReactNode;
  title: string;
  isModalOpen: boolean;
  toggleModal: () => void;
}

const Modal: FC<IModalProps> = ({
  isModalOpen,
  toggleModal,
  children,
  title,
}) => {
  if (!isModalOpen) return null;

  return createPortal(
    <div className={styles.overlay}>
      <div className={styles.modal}>
        <button
          type="button"
          onClick={toggleModal}
          className={styles.modal__button}
          aria-label="close modal window"
        >
          <svg className={styles.modal__button__icon}>
            <use href={dashboardIconsSprite + '#cross'}></use>
          </svg>
        </button>

        <p className={styles.modal__title}>{title}</p>
        {children}
      </div>
    </div>,
    modalRoot,
  );
};

export default Modal;
