import { FC } from 'react';
import { Circles } from 'react-loader-spinner';

import styles from './loader.module.scss';

interface ILoaderProps {
  width?: string | undefined;
  height?: string | undefined;
}

const Loader: FC<ILoaderProps> = ({ height = '100vh', width = '100%' }) => (
  <div className={styles.container} style={{ height, width }}>
    <Circles
      height="80"
      width="80"
      color="#7101ff"
      ariaLabel="circles-loading"
      visible={true}
    />
  </div>
);
export default Loader;
