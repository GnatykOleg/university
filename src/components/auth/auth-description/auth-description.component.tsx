import cnBind from 'classnames/bind';
import { FC } from 'react';

import styles from './auth-description.module.scss';

interface IAuthDescription {
  text: string;
  marginBottom?: number | undefined;
  isCentered?: boolean | undefined;
}

const cnb = cnBind.bind(styles);

const AuthDescription: FC<IAuthDescription> = ({
  text,
  marginBottom,
  isCentered = false,
}) => {
  const authDescriptionClassName = cnb('auth-description', {
    'auth-description--centered': isCentered,
  });

  return (
    <p className={authDescriptionClassName} style={{ marginBottom }}>
      {text}
    </p>
  );
};

export default AuthDescription;
