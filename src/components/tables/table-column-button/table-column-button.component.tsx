import dashboardIconsSprite from 'assets/icons/dashboard-icons-sprite.svg';
import cnBind from 'classnames/bind';
import { FC } from 'react';

import styles from './table-column-button.module.scss';

const cnb = cnBind.bind(styles);

interface ITableColumnButtonProps {
  onEditEntityButtonClick: (id: string) => void;
  id: string;
}

const TableColumnButton: FC<ITableColumnButtonProps> = ({
  onEditEntityButtonClick,
  id,
}) => {
  return (
    <button
      onClick={(): void => onEditEntityButtonClick(id)}
      className={cnb('column-button')}
      type="button"
      aria-label="edit entity button"
    >
      <svg className={cnb('column-button__icon')}>
        <use href={dashboardIconsSprite + '#edit'}></use>
      </svg>
    </button>
  );
};

export default TableColumnButton;
