import { FC, ReactNode } from 'react';

import styles from './table-row.module.scss';

interface ITableRowProps {
  children: ReactNode;
  gridTemplateColumns: string;
}

const TableRow: FC<ITableRowProps> = ({ children, gridTemplateColumns }) => (
  <div className={styles.row} style={{ gridTemplateColumns }}>
    {children}
  </div>
);

export default TableRow;
