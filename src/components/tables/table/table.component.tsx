import { nanoid } from '@reduxjs/toolkit';
import cnBind from 'classnames/bind';
import { FC, ReactNode } from 'react';

import styles from './table.module.scss';

const cnb = cnBind.bind(styles);

interface ITableProps {
  children: ReactNode;
  gridTemplateColumns: string;
  headerColumns: string[];
}

const Table: FC<ITableProps> = ({
  children,
  gridTemplateColumns,
  headerColumns,
}) => {
  return (
    <div className={cnb('table')}>
      <div className={cnb('table__header')} style={{ gridTemplateColumns }}>
        {headerColumns.map((column) => (
          <p key={nanoid()} className={cnb('table__header__text')}>
            {column}
          </p>
        ))}
      </div>

      <div className={cnb('table__children-container')}>{children}</div>
    </div>
  );
};
export default Table;
