import cnBind from 'classnames/bind';
import { FC } from 'react';

import styles from './table-column.module.scss';

interface ITableColumn {
  text: string | null | undefined;
}

const cnb = cnBind.bind(styles);

const TableColumn: FC<ITableColumn> = ({ text }) => {
  const columnText = text ? text : 'no value added';

  const columnClassName = cnb('column', {
    'column--gray': !text,
  });

  return <p className={columnClassName}>{columnText}</p>;
};

export default TableColumn;
