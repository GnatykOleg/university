import dashboardIconsSprite from 'assets/icons/dashboard-icons-sprite.svg';
import { ROUTES_PATHS } from 'utils';

export const navListOptions = [
  {
    text: 'Dashboard',
    href: dashboardIconsSprite + '#dashboard',
    to: '#',
  },
  {
    text: 'Courses',
    href: dashboardIconsSprite + '#courses',
    to: ROUTES_PATHS.COURSES,
  },
  {
    text: 'Lectors',
    href: dashboardIconsSprite + '#lectors',
    to: ROUTES_PATHS.LECTORS,
  },
  {
    text: 'Groups',
    href: dashboardIconsSprite + '#groups',
    to: ROUTES_PATHS.GROUPS,
  },
  {
    text: 'Students',
    href: dashboardIconsSprite + '#students',
    to: ROUTES_PATHS.STUDENTS,
  },
];
