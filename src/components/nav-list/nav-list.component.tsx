import { nanoid } from '@reduxjs/toolkit';
import cnBind from 'classnames/bind';
import { FC } from 'react';
import { NavLink } from 'react-router-dom';
import { clearQueries } from 'redux/queries/queries.slice';
import { useAppDispatch } from 'utils';

import styles from './nav-list.module.scss';
import { navListOptions } from './nav-list.options';

const cnb = cnBind.bind(styles);

const NavList: FC = () => {
  const dispatch = useAppDispatch();

  const onClick = (): void => {
    dispatch(clearQueries());
  };

  return (
    <nav>
      {navListOptions.map(({ text, href, to }, index) => (
        <NavLink
          onClick={onClick}
          key={nanoid()}
          to={to}
          className={({ isActive }): string =>
            cnb('link', {
              'link--active': isActive && index,
              'link--first': !index,
            })
          }
        >
          <svg className={cnb('link__icon')}>
            <use href={href}></use>
          </svg>
          <span className={cnb('link__text')}>{text}</span>
        </NavLink>
      ))}
    </nav>
  );
};

export default NavList;
