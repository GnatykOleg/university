import { Pagination } from '@mui/material';
import { Header, Sidebar } from 'components';
import { ChangeEvent, FC, ReactNode } from 'react';
import { getPage } from 'redux/queries/queries.selectors';
import { setPage } from 'redux/queries/queries.slice';
import { useAppDispatch, useAppSelector } from 'utils';

import styles from './dashboard-layout.module.scss';

interface IDashboardLayoutProps {
  children: ReactNode;
  title: string;
  resourseTotalPages: number;
}

const DashboardLayout: FC<IDashboardLayoutProps> = ({
  children,
  title,
  resourseTotalPages,
}) => {
  const dispatch = useAppDispatch();

  const paginationOnChange = (_: ChangeEvent<unknown>, page: number): void => {
    dispatch(setPage(page));
  };

  const page = useAppSelector(getPage);

  return (
    <div className={styles.wrapper}>
      <Sidebar />

      <div>
        <Header title={title} />
        <main className={styles.main}>
          <section className={styles.section}>
            <h1 className="visually-hidden">Dashboard</h1>

            {children}

            {resourseTotalPages ? (
              <div className={styles.pagination}>
                <Pagination
                  style={{ margin: '0 auto' }}
                  count={resourseTotalPages}
                  page={page}
                  onChange={paginationOnChange}
                />
              </div>
            ) : null}
          </section>
        </main>
      </div>
    </div>
  );
};
export default DashboardLayout;
