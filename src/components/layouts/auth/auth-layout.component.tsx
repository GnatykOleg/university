import threePurpleCubesLogo from 'assets/icons/three-purple-cubes-logo.svg';
import { FC, ReactNode } from 'react';

import styles from './auth-layout.module.scss';

interface IAuthLayoutProps {
  children: ReactNode;
  title: string;
  marginBottomToChildren: number;
}

const AuthLayout: FC<IAuthLayoutProps> = ({
  children,
  title,
  marginBottomToChildren: marginBottom,
}) => (
  <section className={styles.section}>
    <img
      src={threePurpleCubesLogo}
      width={80}
      height={80}
      alt="Three purple cubes logo"
      className={styles.logo}
    />

    <h2 style={{ marginBottom }} className={styles.title}>
      {title}
    </h2>

    {children}
  </section>
);

export default AuthLayout;
