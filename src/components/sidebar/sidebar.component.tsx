import dashboardIconsSprite from 'assets/icons/dashboard-icons-sprite.svg';
import threeWhiteCubeslogo from 'assets/icons/three-white-cubes-logo.svg';
import cnBind from 'classnames/bind';
import NavList from 'components/nav-list/nav-list.component';
import { FC } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { logOut } from 'redux/auth/auth.slice';
import { useAppDispatch } from 'utils';

import styles from './sidebar.module.scss';

const cnb = cnBind.bind(styles);

const Sidebar: FC = () => {
  const navigate = useNavigate();

  const dispatch = useAppDispatch();

  const onLogOutButtonClick = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>,
  ): void => {
    e.preventDefault();

    navigate('/sign-in');

    toast.success('Successful logout!');

    dispatch(logOut());
  };

  return (
    <aside className={cnb('sidebar')}>
      <div className={cnb('sidebar__logo-wrapper')}>
        <img
          src={threeWhiteCubeslogo}
          width={42}
          height={42}
          alt="Three white cubes logo"
        />
      </div>

      <NavList />

      <button onClick={onLogOutButtonClick} className={cnb('logout')}>
        <svg className={cnb('logout__icon')}>
          <use href={dashboardIconsSprite + '#logout'}></use>
        </svg>

        <span className={cnb('logout__text')}>Log out</span>
      </button>
    </aside>
  );
};

export default Sidebar;
