import dashboardIconsSprite from 'assets/icons/dashboard-icons-sprite.svg';
import cnBind from 'classnames/bind';
import { ChangeEvent, FC } from 'react';
import { DebounceInput } from 'react-debounce-input';
import Select, { ActionMeta, MultiValue, SingleValue } from 'react-select';
import {
  getFindByInputValue,
  getFindBySelectSelectedOption,
  getFindBySelectValue,
  getSortBySelectSelectedOption,
} from 'redux/queries/queries.selectors';
import {
  setFindByInputValue,
  setFindBySelectSelectedOption,
  setFindBySelectValue,
  setPage,
  setSortBySelectSelectedOption,
  setSortField,
  setSortOrder,
} from 'redux/queries/queries.slice';
import { IOption } from 'types/api/queries/select-option.interface';
import { useAppDispatch, useAppSelector } from 'utils';

import styles from './dashboard-actions.module.scss';
import { selectStyles } from './select.styles';
import { sortBySelectOptions } from './sort-by-select.options';

const cnb = cnBind.bind(styles);

interface IDashboardActionsProps {
  findByEntityOptions: IOption[];
  whatEntityAddName: string;
  onAddNewEntityButtonClick: () => void;
}

const DashboardActions: FC<IDashboardActionsProps> = ({
  findByEntityOptions,
  whatEntityAddName,
  onAddNewEntityButtonClick,
}) => {
  const dispatch = useAppDispatch();

  const sortBySelectSelectedOption = useAppSelector(
    getSortBySelectSelectedOption,
  );

  const findBySelectSelectedOption = useAppSelector(
    getFindBySelectSelectedOption,
  );

  const findByInputValue = useAppSelector(getFindByInputValue);

  const findBySelectValue = useAppSelector(getFindBySelectValue);

  const sortBySelectOptionsLastElement =
    sortBySelectOptions[sortBySelectOptions.length - 1];

  const findBySelectOptionsLastElement =
    findByEntityOptions[findByEntityOptions.length - 1];

  const sortBySelectCurrentOption = sortBySelectSelectedOption
    ? sortBySelectSelectedOption
    : sortBySelectOptionsLastElement;

  const findBySelectCurrentOption = findBySelectSelectedOption
    ? findBySelectSelectedOption
    : findBySelectOptionsLastElement;

  const onSortBySelectChange = (
    newValue: MultiValue<IOption> | SingleValue<IOption>,
    _: ActionMeta<IOption>,
  ): void => {
    const { label, value } = newValue as IOption;

    dispatch(setSortBySelectSelectedOption({ label, value }));

    switch (true) {
      case label === 'A-Z':
        dispatch(setSortOrder('ASC'));
        dispatch(setSortField('name'));
        break;

      case label === 'Z-A':
        dispatch(setSortOrder('DESC'));
        dispatch(setSortField('name'));
        break;

      case label === 'Oldest first':
        dispatch(setSortOrder('ASC'));
        dispatch(setSortField('createdAt'));
        break;

      case label === 'Newest first':
        dispatch(setSortOrder('DESC'));
        dispatch(setSortField('createdAt'));
        break;

      case label === 'All':
        dispatch(setSortOrder(undefined));
        dispatch(setSortField(undefined));
        break;

      default:
        break;
    }
  };

  const onFindBySelectChange = (
    newValue: MultiValue<IOption> | SingleValue<IOption>,
    _: ActionMeta<IOption>,
  ): void => {
    const { value, label } = newValue as IOption;

    dispatch(setFindBySelectSelectedOption({ value, label }));

    dispatch(setFindBySelectValue(value));
  };

  const onFindByInputChange = (e: ChangeEvent<HTMLInputElement>): void => {
    const { value } = e.target;

    if (!findBySelectValue)
      dispatch(setFindBySelectValue(findBySelectOptionsLastElement.value));

    dispatch(setPage(1));

    dispatch(setFindByInputValue(value));
  };

  return (
    <div className={cnb('wrapper')}>
      <div className={cnb('select-wrapper')}>
        <p className={cnb('select__label')}>Sort by</p>
        <Select
          value={sortBySelectCurrentOption}
          onChange={onSortBySelectChange}
          options={sortBySelectOptions}
          isSearchable={false}
          styles={selectStyles}
        />
      </div>

      <div className={cnb('select-wrapper')}>
        <p className={cnb('select__label')}>Find by</p>
        <Select
          value={findBySelectCurrentOption}
          onChange={onFindBySelectChange}
          options={findByEntityOptions}
          isSearchable={false}
          styles={selectStyles}
        />
      </div>

      <div className={cnb('input-wrapper')}>
        <DebounceInput
          className={cnb('input')}
          debounceTimeout={500}
          value={findByInputValue}
          type="text"
          placeholder="Search"
          autoComplete="off"
          onChange={onFindByInputChange}
        />

        <svg className={cnb('input__icon')}>
          <use href={dashboardIconsSprite + '#search'}></use>
        </svg>
      </div>

      <button onClick={onAddNewEntityButtonClick} className={cnb('button')}>
        <svg className={cnb('button__icon')}>
          <use href={dashboardIconsSprite + '#plus'}></use>
        </svg>
        {`Add new ${whatEntityAddName.toLowerCase()}`}
      </button>
    </div>
  );
};

export default DashboardActions;
