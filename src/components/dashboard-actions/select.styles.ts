import { StylesConfig } from 'react-select';
import { IOption } from 'types/api/queries/select-option.interface';

export const selectStyles: StylesConfig<IOption> = {
  container: () => ({
    position: 'relative',
    width: 140,
    height: 38,

    color: '#3B4360',
    fontFamily: 'Nunito',
    fontWeight: 500,
    lineHeight: 1.7,
  }),

  control: (_, state) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',

    maxHeight: '100%',

    padding: '7px 6px 7px 12px',

    borderTopRightRadius: 6,
    borderTopLeftRadius: 6,
    borderBottomRightRadius: state.menuIsOpen ? 0 : 6,
    borderBottomLeftRadius: state.menuIsOpen ? 0 : 6,

    border: '1px solid #E4E7ED',
    backgroundColor: '#FFF',
  }),

  valueContainer: () => ({
    '> input': {
      display: 'none',
    },
  }),

  singleValue: () => ({ pointerEvents: 'none' }),

  menu: () => ({
    width: '100%',

    position: 'absolute',
    top: '100%',

    zIndex: 1,

    padding: '8px 0px 7px 12px',

    backgroundColor: '#FFF',

    borderBottom: '1px solid #E4E7ED',
    borderLeft: '1px solid #E4E7ED',
    borderRight: '1px solid #E4E7ED',

    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6,
  }),

  menuList: () => ({}),

  option: (_, state) => ({
    transition: 'all 0.6s ease',

    cursor: 'pointer',

    color: state.isSelected ? '#3B4360' : 'rgba(59, 67, 96, 0.45)',

    ':not(:last-child)': {
      marginBottom: 8,
    },

    ':hover': {
      color: '#3B4360',
    },
  }),

  indicatorsContainer: () => ({ cursor: 'pointer' }),

  dropdownIndicator: (_, state) => ({
    display: 'flex',
    alignItems: 'center',
    transform: state.selectProps.menuIsOpen ? 'rotate(180deg)' : 'none',
  }),
};
