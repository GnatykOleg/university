import { IOption } from 'types/api/queries/select-option.interface';

export const sortBySelectOptions: IOption[] = [
  { value: 'name_asc', label: 'A-Z' },
  { value: 'name_desc', label: 'Z-A' },
  { value: 'created_at_asc', label: 'Oldest first' },
  { value: 'created_at_desc', label: 'Newest first' },
  { value: 'all', label: 'All' },
];
