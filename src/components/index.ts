// Layouts
export { default as AuthLayout } from './layouts/auth/auth-layout.component';
export { default as DashboardLayout } from './layouts/dashboard/dashboard-layout.component';

// Components => Common
export { default as Button } from './common/button/button.component';
export { default as Checkbox } from './common/checkbox/checkbox.component';
export { default as Error } from './common/error/error.component';
export { default as GoBackButton } from './common/go-back-button/go-back-button.component';
export { default as Input } from './common/input/input.component';
export { default as Loader } from './common/loader/loader.component';
export { default as Modal } from './common/modal/modal.component';

// Components => Forms
export { default as ForgotPasswordForm } from './forms/auth/forgot-password-form.component';
export { default as ResetPasswordForm } from './forms/auth/reset-password-form.component';
export { default as SignInForm } from './forms/auth/sign-in-form.component';
export { default as SignUpForm } from './forms/auth/sign-up-form.component';
export { default as BaseForm } from './forms/base-form/base-form.component';
export { default as CreateStudentForm } from './forms/dashboard/add-student-form.component';
export { default as CourseForm } from './forms/dashboard/course-form.component';
export { default as GroupForm } from './forms/dashboard/group-form.component';
export { default as LectorForm } from './forms/dashboard/lector-form.component';
export { default as UpdateStudentForm } from './forms/dashboard/update-student-form.component';

// Components
export { default as AuthDescription } from './auth/auth-description/auth-description.component';
export { default as DashboardActions } from './dashboard-actions/dashboard-actions.component';
export { default as Header } from './header/header.component';
export { default as NavList } from './nav-list/nav-list.component';
export { default as Sidebar } from './sidebar/sidebar.component';
export { default as Table } from './tables/table/table.component';
export { default as TableColumn } from './tables/table-column/table-column.component';
export { default as TableColumnButton } from './tables/table-column-button/table-column-button.component';
export { default as TableRow } from './tables/table-row/table-row.component';
