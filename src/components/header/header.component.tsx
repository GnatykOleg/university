import notification from 'assets/icons/notification.svg';
import threePurpleCubeslogo from 'assets/icons/three-purple-cubes-logo.svg';
import avatar from 'assets/images/avatar.jpg';
import cnBind from 'classnames/bind';
import { FC } from 'react';

import styles from './header.module.scss';

interface IHeaderProps {
  isPrimaryHeader?: boolean;
  title: string;
}

const cnb = cnBind.bind(styles);

const Header: FC<IHeaderProps> = ({ isPrimaryHeader = true, title }) => {
  const isNotificationActive = true;

  const headerClassName = cnb('header', {
    'header--secondary': !isPrimaryHeader,
  });

  const headerTittleClassName = cnb('header__title');

  const headerLogoClassName = cnb('header__logo');

  const notificationClassName = cnb('notification', {
    'notification--active': isNotificationActive,
  });

  const headerAvatar = cnb('header__avatar');

  const notificationIconClassName = cnb('notification__icon');

  return (
    <header className={headerClassName}>
      {isPrimaryHeader && <h2 className={headerTittleClassName}>{title}</h2>}

      {!isPrimaryHeader && (
        <img
          className={headerLogoClassName}
          src={threePurpleCubeslogo}
          width={42}
          height={42}
          alt="Three purple cubes logo"
        />
      )}

      <button className={notificationClassName} aria-label="notification">
        <svg className={notificationIconClassName}>
          <use href={notification + '#notification'}></use>
        </svg>
      </button>

      <img
        className={headerAvatar}
        src={avatar}
        alt="user avatar"
        width={32}
        height={32}
      />
    </header>
  );
};

export default Header;
